# BlueDroplet #

(Dropwizard for RPG)

For some time now there is the trend for packaging REST services into a 
fat executable ready to deploy anywhere without much or any configuration.   
[Dropwizard](https://dropwizard.github.io/dropwizard/) is such a project. 
It takes best of breed stuff to create an easy to use platform for 
developing REST services.

Developing REST services on the IBM i server has been very hard and/or 
complicated. Creating such a platform would ease the integration of the 
IBM i platform into the rest of the IT application infrastructure.

## Web Server ##
Of course we need a very stable web server for this project but to get 
things rolling simplicity over stability as we need to be able to create 
an ILE module out of the source code from the web server. And IBM i is 
not known for compatible C or C++ code compared to Linux C or C++ code.

Mongoose from Cesanta is used for the first steps, see https://github.com/cesanta/mongoose.

## Building ##
The project stores the source code in the IFS in stream files. The make
tool is used to build the project. BIN_LIB and INCLUDE are variables in the
Makefile which can be overwritten with your own values.

BIN_LIB: the library for modules and binding directory

INCLUDE: the IFS folder for the header files / copy books

```
make BIN_LIB=MSCHMIDT INCLUDE=/usr/local/include
```


## Status
See the [Status](https://bitbucket.org/m1hael/bluedroplet/wiki/Status) wiki article of what does already work and what is not yet implemented.


## Contribution Guidelines ##

You can contribute in any way you like. Just do it! =)

Take a look at the wiki to get into the project.


## Who do I talk to? ##

Mihael Schmidt , mihael@rpgnextgen.com
