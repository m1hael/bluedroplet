**FREE

///
// @brief BlueDroplet : Router Longest Path
//
// This module contains procedures for routing HTTP request. It routes
// the request to the endpoint which matches most of the path but does not 
// need to match the full path.
//
// @author Mihael Schmidt
// @date   26.02.2017
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2017 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'service_t.rpgle'
/include 'router_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'router_longest_path_h.rpgle'
/include 'llist/llist_h.rpgle'
/include 'libc_h.rpgle'
/include 'endpoint_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------

///
// Create longest matching path router
//
// \return router instance
///
dcl-proc droplet_router_longest_create export;
  dcl-pi *N pointer;
    userData pointer const options(*nopass);
  end-pi;

  dcl-s router pointer;
  dcl-ds routerDs likeds(droplet_router_t) based(router);
  
  router = %alloc(%size(droplet_router_t));
  routerDs.id = 'droplet_router_longest_path';
  routerDs.proc_route = %paddr('droplet_router_longest_route');
  routerDs.proc_finalize = %paddr('droplet_router_longest_finalize');
  
  return router;
end-proc;


///
// \brief Route to end point with longest matching path
//
// Matching end points have the requirements:
// <ul>
//   <li>HTTP method matches</li>
//   <li>URL path matches</li>
// </ul>
//
// \param list of end points (endPoint_t)
// \param HTTP method
// \param HTTP path
//
// \return matching end point or <code>*null</code> if no end point matches
///
dcl-proc droplet_router_longest_route export;
  dcl-pi *N pointer;
    router pointer const;
    endPoints pointer const;
    method char(8) const;
    parmPath char(1024) const;
  end-pi;
  
  dcl-s path char(1024);
  dcl-s endPointPathLength int(10);
  dcl-s pathLength int(10);
  dcl-s matchedLength int(10);
  dcl-s matchingEndPoint pointer;
  dcl-ds endpoint likeds(endpoint_t) based(ptr);
  dcl-s matches ind inz(*off);
  dcl-s c char(1) based(charPtr);
  
  path = parmPath;
  pathLength = %len(%trimr(path));
  
  ptr = list_iterate(endPoints);
  dow (ptr <> *null);
  
    // test for matching HTTP method
    if (endPoint.method = method);
    
      endPointPathLength = %len(%trimr(endPoint.path));
      if (endPointPathLength > pathLength);
        // cannot match
      elseif (memcmp(%addr(endPoint.path) : %addr(path) : endPointPathLength) = 0);
        
        if (endPointPathLength = pathLength);
          matches = *on;
        else;
          
          // path     : /mdm/items/123/45
          // endpoint : /mdm/item
          //            /mdm
          // ----------------------------
          // correct  : /mdm
          
          // check if the next character from the request path is a slash / 
          // => a slash as a next char means that the match matches a full segment => match
          
          // pointer moved to next char of request path
          charPtr = %addr(path) + endPointPathLength;
          if (c = '/');
            matches = *on;
          endif;
          
        endif;
        
        if (matches = *on);
          // now check if this endpoint path is longer than the last one which matched
          if (endPointPathLength > matchedLength);
            matchedLength = endPointPathLength;
            matchingEndPoint = ptr;
          endif;
        endif;
        
      endif;
      
    endif;
    
    matches = *off;
    ptr = list_iterate(endPoints);
  enddo;
    
  return matchingEndPoint;
end-proc;


dcl-proc droplet_router_longest_finalize export;
  dcl-pi *N;
    router pointer;
  end-pi;
  
  if (router <> *null);
    dealloc(n) router;
  endif;
  
end-proc;