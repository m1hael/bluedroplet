**FREE

/if not defined (BLUEDROPLET_HTTP_H)
/define BLUEDROPLET_HTTP_H

//
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//


/include 'util_h.rpgle'


//
// Tepmlates
//
dcl-ds http_queryPart_t qualified template;
  key char(1024);
  value char(1024);
end-ds;


//
// Prototypes
//
dcl-pr droplet_http_getMethod char(10) extproc('droplet_http_getMethod');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getUri char(1024) extproc('droplet_http_getUri');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getProtocol char(10) extproc('droplet_http_getProtocol');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getResponseCode int(10) extproc('droplet_http_getResponseCode');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getResponseStatusMessage varchar(32000) extproc('droplet_http_getResponseStatusMessage');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getQueryString likeds(droplet_buffer) extproc('droplet_http_getQueryString');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getHeader char(1024) extproc('droplet_http_getHeader');
  httpMessage pointer const;
  headerName char(100) const;
end-pr;

dcl-pr droplet_http_getHeaderNames pointer extproc('droplet_http_getHeaderNames');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getBody likeds(droplet_buffer) extproc('droplet_http_getBody');
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_getQueryValue char(1024) extproc(*dclcase);
  httpMessage pointer const;
  key char(1024) const;
end-pr;

dcl-pr droplet_http_getQueryValues pointer extproc(*dclcase);
  httpMessage pointer const;
end-pr;

dcl-pr droplet_http_containsQueryKey ind extproc(*dclcase);
  httpMessage pointer const;
  key char(1024) const;
end-pr;

dcl-pr droplet_http_getPathElement char(1024) extproc(*dclcase);
  httpMessage pointer const;
  index uns(10) const;
end-pr;

/endif
