**FREE

///
// @brief BlueDroplet : Simple Example
//
// This example shows how to compose and start the server and how
// to register a REST end point at the server.
//
// @author Mihael Schmidt
// @date   23.11.2016
//
///


// ctl-opt dftactgrp(*no) actgrp(*caller) thread(*concurrent);
ctl-opt thread(*concurrent);

//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet_h.rpgle'

dcl-pr main end-pr;

dcl-pr mdm_getItem extproc('mdm_getItem');
  service pointer const;
  connection pointer const;
  message pointer const;
  endpoint pointer const;
end-pr;


//-------------------------------------------------------------------------------------------------
// Program Entry Point
//-------------------------------------------------------------------------------------------------
main();
*inlr = *on;
return;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc main;
  dcl-s service pointer;

  service = droplet_service_create();
  dsply 'created';

  // droplet_service_setLogLevel(service : DROPLET_LOG_LEVEL_DEBUG);

  droplet_service_addEndPoint(service : %paddr('mdm_getItem') : '/mdm/item' : DROPLET_GET);
  dsply 'end point added';

  dsply 'starting ...';
  droplet_service_start(service);
  dsply 'ended';

  droplet_service_finalize(service);
  dsply 'cleaned up';
end-proc;


///
// \brief Get item data
//
// This method returns static item data.
//
// \author Mihael Schmidt
// \date   23.11.2016
//
///
dcl-proc mdm_getItem export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
    endpoint pointer const;
  end-pi;

  dcl-s data varchar(100);
  data = '{ "item" : 5500 }';

  droplet_service_send(connection : DROPLET_OK : data : DROPLET_JSON);
end-proc;
