#include "mongoose.h"
#include <qtqiconv.h>

char * convertValue(iconv_t cd, char * value, unsigned int length);

struct applicationUserData {
  iconv_t cdEbcdicToIso;
  iconv_t cdIsoToEbcdic;
};

static const char *s_http_port = "35800";
static struct mg_serve_http_opts s_http_server_opts;

static const char *data = "{ 'id' : 101 }";
static QtqCode_T isoCode, ebcdicCode;

static void ev_handler(struct mg_connection *connection, int ev, void *p) {
	
  printf("%d\n", ev);
  
  if (ev == MG_EV_ACCEPT) {
    
    // initialize user data structure
    struct applicationUserData userData;
    
    // create iconv descriptor
    isoCode.CCSID = 819;
    isoCode.cnv_alternative = 0;
    isoCode.subs_alternative = 0;
    isoCode.shift_alternative = 1;
    isoCode.length_option = 0;
    isoCode.mx_error_option = 0;
    memset(isoCode.reserved, 0, 8);

    ebcdicCode.CCSID = 37;
    ebcdicCode.cnv_alternative = 0;
    ebcdicCode.subs_alternative = 0;
    ebcdicCode.shift_alternative = 1;
    ebcdicCode.length_option = 0;
    ebcdicCode.mx_error_option = 0;
    memset(ebcdicCode.reserved, 0, 8);
    
    iconv_t cdIsoToEbcdic = QtqIconvOpen(&ebcdicCode, &isoCode);
    if(cdIsoToEbcdic.return_value == -1) {
        if (errno == EINVAL) {
            fprintf(stderr,
                "Conversion is not supported\n");
                // TODO send HTTP code 500
        } else {
            fprintf(stderr, "Initialization failure:\n"); // TODO output error string
            // TODO send HTTP code 500
        }   
    }
    else {
      // add conversion descriptor to user data
      userData.cdIsoToEbcdic = cdIsoToEbcdic;
    }
     
    iconv_t cdEbcdicToIso = QtqIconvOpen(&isoCode, &ebcdicCode);
    if(cdEbcdicToIso.return_value == -1) {
        if (errno == EINVAL) {
            fprintf(stderr,
                "Conversion is not supported\n");
                // TODO send HTTP code 500
        } else {
            fprintf(stderr, "Initialization failure:\n"); // TODO output error string
            // TODO send HTTP code 500
        }   
    }
    else {
      // add conversion descriptor to user data
      userData.cdEbcdicToIso = cdEbcdicToIso;
    }
    
    connection->user_data = &userData;
  }
  else if (ev == MG_EV_RECV) {
    // convert data to EBCDIC
    // see if there is some user data
    if (connection->user_data == NULL) {
      fprintf(stderr, "No iconv descriptor to convert values on data receive event.\n");
    }
    else {
      struct applicationUserData *userData = connection->user_data;
      iconv_t cd = userData->cdIsoToEbcdic;
      
      // TODO handle large data (multiple RECV event, data will be APPENDED to buffer)
      struct mbuf * buffer = &connection->recv_mbuf;
      char * value = buffer->buf;
      int length = buffer->len;
      printf("Input network (%d bytes)\n", length);
      char * convertedValue = convertValue(cd, value, length);
      mbuf_remove(buffer, buffer->len);
      int convertedLength = strlen(convertedValue);
      printf("Input converted (%d bytes)\n", convertedLength);
      mbuf_append(buffer, convertedValue, convertedLength);
    }
  }
  else if (ev == MG_EV_CLOSE) {
    // see if there is some user data
    if (connection->user_data != NULL) {
    
      // close iconv descriptor
      struct applicationUserData *userData = connection->user_data;
      
      iconv_t cd = userData->cdEbcdicToIso;
      iconv_close(cd);
      cd = userData->cdIsoToEbcdic;
      iconv_close(cd);
      
      // remove user data
      connection->user_data = NULL; 
    }
  }
  else if (ev == MG_EV_HTTP_REQUEST) {
	printf("eventhandler: receiving http request\n");
    
    // char * returnData = calloc(1024, 1);
    // int length = sprintf(returnData, "HTTP/1.1 200 OK\r\n"
    //             "Content-Type: text/plain\r\n"
    //             "Content-Length: %d\r\n\r\n%s",
    //             (int) strlen(data), data);

    // struct applicationUserData *userData = connection->user_data;
    // iconv_t cd = userData->cdEbcdicToIso;
    // char * convertedValue = convertValue(cd, returnData, &length);
    
    // mg_send(connection, returnData, length);
    // mg_send(connection, convertedValue, length);
    
    // free(returnData);
    // free(convertedValue);
    
    // \n = newline was translated to x85 which is not correct, should be x0A
    
    char * value = "\x48\x54\x54\x50\x2F\x31\x2E\x31\x20\x32\x30\x30\x20\x4F\x4B\x0D"
                   "\x0A\x43\x6F\x6E\x74\x65\x6E\x74\x2D\x54\x79\x70\x65\x3A\x20\x74"
                   "\x65\x78\x74\x2F\x70\x6C\x61\x69\x6E\x0D\x0A\x43\x6F\x6E\x74\x65"
                   "\x6E\x74\x2D\x4C\x65\x6E\x67\x74\x68\x3A\x20\x31\x34\x0D\x0A\x0D"
                   "\x0A\x7B\x20\x27\x69\x64\x27\x20\x3A\x20\x31\x30\x31\x20\x7D\x00";
    unsigned int length = strlen(value);
    printf("Sending %d bytes\n", length);
    mg_send(connection, value, length);
    
    printf("Data sent\n");
    struct mbuf *io = &connection->recv_mbuf;
    mbuf_remove(io, io->len);
    printf("Buffer flushed\n");
  }
}

char * convertValue(iconv_t cd, char * value, unsigned int length) {

    // the converted string can be four times larger
    // then the original, as the largest known char width is 4 bytes.    
    char *converted = calloc(length * 4 + 1, sizeof(char));

    // we need to store an additional pointer that targets the
    // start of converted. (iconv modifies the original 'converted')
    char *converted_start = converted;

    size_t ibl = length + 1; // len of iso
    size_t obl = length * 4 + 1; // len of converted

    // do it!
    int ret = iconv(cd, &value, &length, &converted, &obl);

    // if iconv fails it returns -1
    if(ret == -1) {
        perror("iconv");
        free(converted);
        return NULL;
    } else {
        // other wise the number of converted bytes
        printf("%i bytes converted\n", ret);
        printf("result: '%s'\n", converted_start);
        return converted_start;
    }   
}

int main(void) {

  struct mg_mgr mgr;
  struct mg_connection *connection;

  mg_mgr_init(&mgr, NULL);
  printf("Starting web server on port %s\n", s_http_port);
  connection = mg_bind(&mgr, s_http_port, ev_handler);
  if (connection == NULL) {
    printf("Failed to create listener\n");
    return 1;
  }

  // Set up HTTP server parameters
  mg_set_protocol_http_websocket(connection);

  int i = 0;
  for (i = 0; i < 10; i++) {
    mg_mgr_poll(&mgr, 3000);
  }
  mg_mgr_free(&mgr);

  return 0;
}
