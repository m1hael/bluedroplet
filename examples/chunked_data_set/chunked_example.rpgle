**FREE

///
// @brief BlueDroplet : Send Chunked Data
//
// This example demonstrates how to send large data sets as HTTP chunks. This
// is very convenient if you want to return list of entries and you don't know
// at the beginning how large the data set is going to be.
//
// @author Mihael Schmidt
// @date   05.12.2016
///


ctl-opt dftactgrp(*no) actgrp(*caller);


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/bluedroplet_h.rpgle'
/include 'bluedroplet/mongoose_h.rpgle'
/include 'llist_h.rpgle'


dcl-pr main end-pr;

dcl-pr mdm_listItems extproc('mdm_listItems');
  service pointer const;
  connection pointer const;
  message pointer const;
  endpoint pointer const;
end-pr;


//-------------------------------------------------------------------------------------------------
// Program Entry Point
//-------------------------------------------------------------------------------------------------
main();
*inlr = *on;
return;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc main;
  dcl-s service pointer;
  dcl-ds configuration likeds(droplet_config_configuration);

  service = droplet_service_create();

  droplet_service_addEndPoint(service : %paddr('mdm_listItems') : '/mdm/item' : DROPLET_GET);

  droplet_service_start(service);

  droplet_service_finalize(service);
end-proc;


///
// \brief List item data
//
// This method returns a list of item data to the caller of the REST service
// in JSON format.
///
dcl-proc mdm_listItems export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
    endpoint pointer const;
  end-pi;

  dcl-s data varchar(200);
  dcl-s headers pointer;
  headers = list_create();
  list_addString(headers : 'Transfer-Encoding: chunked');

  droplet_service_sendHead(connection : DROPLET_OK : DROPLET_JSON : headers);

  data = '{ ''items'' : [ ';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''EP0700M06'' , ' +
           '''description'' : ''CTP 7.0", 2D Multi-Touch, FocalTech'' , ' +
           '''quantity'' : 26 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''ET024012DMA'' , ' +
           '''description'' : ''TFT 2.4" QVGA, 350cd/m LED MCU interface'' , '+
           '''quantity'' : 338 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''ET1010G0DSA'' , ' +
           '''description'' : ''TFT 10.1" WXGA, 800 cd/m LED, IPS, LVDS, ' +
           'Glyn specified PCB'' , ''quantity'' : 412 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''ETML101001DKA'' , ' +
           '''description'' : ''TFT 10.1" WXGA, 680 cd/m LED, IPS, LVDS, ' +
           'cap. Touch'' , ''quantity'' : 14 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''ETML1010G0DKA'' , ' +
           '''description'' : ''TFT 10.1" WXGA, 680 cd/m LED, IPS, LVDS, ' +
           'cap. Touch, Glyn specified PCB'' , ''quantity'' : 2 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''EW13B36BMW'' , ' +
           '''description'' : ''LCD 128 x 64, STN blue, white LED'' , ' +
           '''quantity'' : 11 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''EW13B36FLW'' , ' +
           '''description'' : ''LCD 128 x 64, FSTN positive, white LED'' , ' +
           '''quantity'' : 154 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = '{ ''manufacturer'' : ''EDT'' , ''item'' : ''EW16290YLY'' , ' +
           '''description'' : ''LCD 16 x 2, STN green, yel/green LED'' , ' +
           '''quantity'' : 13 }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  data = ' ] }';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  // signal the end of the data set by sending an empty chunk.
  data = '';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  droplet_service_setFlag(connection : MONGOOSE_FLAG_SEND_AND_CLOSE);
end-proc;
