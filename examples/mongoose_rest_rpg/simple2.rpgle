**FREE

//
// \brief Mongoose Test in RPG
//
// \author Mihael Schmidt
// \date   01.11.2016
//

//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
/include 'mongoose_h.rpgle'
/include 'iconv_h.rpgle'
/include 'message_h.rpgle'
/include 'libc_h.rpgle'
      
      
//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr main end-pr;
      
dcl-pr eventHandler extproc('eventHandler');
    connection pointer value;
    event int(10) value;
    userData pointer value;
end-pr;
     
dcl-pr translateToAscii;
    string pointer;
    length uns(10) const;
end-pr;

dcl-pr translateFromAscii;
    string pointer;
    length uns(10) const;
end-pr;

     
//-------------------------------------------------------------------------------------------------
// Program Entry Point
//-------------------------------------------------------------------------------------------------
main();
*inlr = *on;
return;
     
     
//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc main;
    dcl-s manager pointer;
    dcl-s connection pointer;
    dcl-s i int(10);
    
    manager = %alloc(80); // manager struct has 80 bytes
           
    mongoose_manager_init(manager : *null);
    dsply 'Starting web server';
           
    connection = mongoose_bind(manager : '35800' : %paddr('eventHandler'));
    if (connection = *null);
        dsply 'Failed to create listener';
        return;
    endif;
           
    // Set up HTTP server parameters
    mongoose_set_protocol_http_websocket(connection);
           
    for i = 0 to 3;
        mongoose_manager_poll(manager : 3000);
    endfor;
           
    mongoose_manager_free(manager);
    
    dealloc manager;
           
    return;
end-proc;
     
     
//
// \brief Callback procedure for web server
//
// This procedure will be called by the Mongoose web server for any event.
// Event types are described as constants in the copybook mongoose_h.rpgle.
//
// It will return some static data regardless of the URL path or parameters.
//
dcl-proc eventHandler export;
    dcl-pi *n;
        connection pointer value;
        event int(10) value;
        userData pointer value;
    end-pi;
    
    dcl-s data char(20) inz('{ "id" : 5500 }');
    dcl-s ptr pointer inz(%addr(data));
    dcl-s message char(50);
    dcl-ds connectionDs likeds(mongoose_connection) based(connection);
    dcl-s translationStorage pointer;
    dcl-s translatedLength int(10);
    
    message = 'event type ' + %char(event);
    dsply message;
    
    if (event = MONGOOSE_EVENT_RECEIVED);
      translationStorage = %alloc(connectionDs.receivingBuffer.length + 1);
      memcpy(translationStorage : connectionDs.receivingBuffer.buffer : connectionDs.receivingBuffer.length);
      ptr = translationStorage;
      translateFromAscii(ptr : connectionDs.receivingBuffer.length);
      translatedLength = strlen(translationStorage);
      
      mongoose_buffer_remove(%addr(connectionDs.receivingBuffer) : connectionDs.receivingBuffer.length);
      mongoose_buffer_append(%addr(connectionDs.receivingBuffer) : translationStorage : translatedLength);
      
      dealloc(n) translationStorage;
      
    elseif (event = MONGOOSE_EVENT_HTTP_REQUEST);
        ptr = %addr(data);
        translateToAscii(ptr : 15);
        ptr = %addr(data);
        mongoose_send(connection : ptr : 15);
        mongoose_buffer_remove(%addr(connectionDs.receivingBuffer) : connectionDs.receivingBuffer.length);
    endif;
    
end-proc;


dcl-proc translateToAscii;
    dcl-pi *N;
        string pointer;
        pLength uns(10) const;
    end-pi;
    
    dcl-ds iconv_table likeds(iconv_t) static;
    dcl-s isInit ind inz(*off) static;
    dcl-s length uns(10);
          
    dcl-ds iconv_from qualified;
        ccsid int(10) inz(0);
        convAlt int(10) inz(0);
        subsAlt int(10) inz(0);
        shiftAlt int(10) inz(1);
        inpLenOp int(10) inz(0);
        errorOpt int(10) inz(1);
        reserved char(8) inz(*ALLx'00');
    end-ds;
    
    dcl-ds iconv_to qualified;
        ccsid  int(10) inz(819);
        convAlt int(10) inz(0);
        subsAlt int(10) inz(0);
        shiftAlt int(10) inz(1);
        inpLenOp int(10) inz(0);
        errorOpt int(10) inz(1);
        reserved char(8) inz(*ALLx'00');
    end-ds;

    length = pLength;
    if (not isInit);
       iconv_table = iconv_open(iconv_to : iconv_from);
       if (iconv_table.return_value = -1);
           msg_sendEscapeMessage('Could not init iconv data structure.');
       endif;
    endif;
    if (iconv(iconv_table : string : length : string : length) = -1);
        msg_sendEscapeMessage('Error converting data.');
    endif;
    
    iconv_close(iconv_table);
end-proc;


dcl-proc translateFromAscii;
    dcl-pi *N;
        string pointer;
        pLength uns(10) const;
    end-pi;
    
    dcl-ds iconv_table likeds(iconv_t) static;
    dcl-s isInit ind inz(*off) static;
    dcl-s length uns(10);
          
    dcl-ds iconv_from qualified;
        ccsid int(10) inz(819);
        convAlt int(10) inz(0);
        subsAlt int(10) inz(0);
        shiftAlt int(10) inz(1);
        inpLenOp int(10) inz(0);
        errorOpt int(10) inz(1);
        reserved char(8) inz(*ALLx'00');
    end-ds;
    
    dcl-ds iconv_to qualified;
        ccsid  int(10) inz(0);
        convAlt int(10) inz(0);
        subsAlt int(10) inz(0);
        shiftAlt int(10) inz(1);
        inpLenOp int(10) inz(0);
        errorOpt int(10) inz(1);
        reserved char(8) inz(*ALLx'00');
    end-ds;

    length = pLength;
    if (not isInit);
        iconv_table = iconv_open(iconv_to : iconv_from);
        if (iconv_table.return_value = -1);
            msg_sendEscapeMessage('Could not init iconv data structure.');
        endif;
    endif;
    if (iconv(iconv_table : string : length : string : length) = -1);
        msg_sendEscapeMessage('Error converting data.');
    endif;
    iconv_close(iconv_table);
end-proc;
