**FREE

/if not defined (BLUEDROPLET_UTIL_H)
/define BLUEDROPLET_UTIL_H

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
dcl-ds droplet_buffer qualified template;
  string pointer;
  length uns(10);
end-ds;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'iconv_h.rpgle'


///
// Create iconv descriptor
//
// Creates an iconv descriptor and opens the iconv conversion using the created
// descriptor. It needs to be closed after usage.
//
// \param from CCSID
// \param to CCSID
// 
// \return iconv descriptor (iconv_t data structure)
///
dcl-pr droplet_util_createConversionDescriptor likeds(iconv_t) extproc(*dclcase);
  fromCcsid int(10) const;
  toCcsid int(10) const;
end-pr;

///
// Get Charset from HTTP Header
//
// Parses the passed HTTP header entry and returns the charset if present.
//
// \param HTTP header entry
// \param HTTP header entry length
// 
// \return iconv descriptor (iconv_t data structure)
///
dcl-pr droplet_util_getCharsetFromHttpHeader char(50) extproc('droplet_util_getCharsetFromHttpHeader');
  headerEntry char(100) const;
end-pr;

/endif
