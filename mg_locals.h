#define CS_PLATFORM CS_P_OS400
#define _XOPEN_SOURCE 520
#define MG_DISABLE_FILESYSTEM

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <ifs.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>

typedef int sock_t;
typedef unsigned long uintptr_t;

#define INVALID_SOCKET (-1)
#define closesocket(x) close(x)
#define to64(x) strtoll(x, NULL, 10)

#define __BIG_ENDIAN 1
#define BIG_ENDIAN __BIG_ENDIAN
#define BYTE_ORDER BIG_ENDIAN

// multithreading
#define MG_ENABLE_THREADS 1
#include <pthread.h>