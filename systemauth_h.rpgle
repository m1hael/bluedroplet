**FREE

/if not defined (BLUEDROPLET_AUTH_SYSTEM)
/define BLUEDROPLET_AUTH_SYSTEM


//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

dcl-pr droplet_auth_system_create pointer extproc(*dclcase);
  userData pointer const options(*nopass : *string);
end-pr;
  
dcl-pr droplet_auth_system_authenticate ind extproc(*dclcase);
  authenticator pointer const;
  credentials pointer const;
  type int(5) const;
end-pr;
  
dcl-pr droplet_auth_system_finalize extproc(*dclcase);
  authenticator pointer;
end-pr;

/endif
