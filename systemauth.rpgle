**FREE

///
// \brief BlueDroplet : System Authenticator
//
// This module implements authentication via user profile check on the current system.
//
// \author Mihael Schmidt
// \date   28.08.2018
//
///

//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

ctl-opt nomain;


//
// Prototypes
//
/include 'auth_h.rpgle'
/include 'libc_h.rpgle'
/include 'message_h.rpgle'
/include 'systemauth_h.rpgle'
/if not defined(QUSEC)
/define QUSEC
/include QSYSINC/QRPGLESRC,QUSEC
/endif

dcl-pr system_getProfileHandle extpgm('QSYGETPH');
  userid char(10) const;
  password char(256) options(*varsize) const;
  profileHandle char(12);
  errorCode likeds(QUSEC) options(*nopass);
  passwordLength int(10) const options(*nopass);
  passwordCcsid int(10) const options(*nopass);
end-pr;

dcl-pr system_releaseProfileHandle extpgm('QSYRLSPH');
  profileHandle char(12) const;
end-pr;


//
// Procedures
//
dcl-proc droplet_auth_system_create export;
  dcl-pi *N pointer;
    userData pointer const options(*nopass : *string);
  end-pi;
  
  dcl-s authenticator pointer;
  dcl-ds authDs likeds(authenticator_t) based(authenticator);
  
  authenticator = %alloc(%size(authenticator_t));
  authDs.id = 'droplet_auth_system';
  authDs.proc_authenticate = %paddr('droplet_auth_system_authenticate');
  authDs.proc_finalize = %paddr('droplet_auth_system_finalize');
  
  if (%parms() > 0 and userData <> *null);
    authDs.userData = %alloc(strlen(userData) + 1);
    strcpy(authDs.userData : userData);
  endif;
  
  return authenticator;
end-proc;

dcl-proc droplet_auth_system_authenticate export;
  dcl-pi *n ind;
    authenticator pointer const;
    credentials pointer const;
    type int(5) const;
  end-pi;
  
  dcl-ds basicAuthDs likeds(droplet_auth_basic_t) based(credentials);
  dcl-s handle char(12);
  
  if (type <> DROPLET_AUTH_BASIC);
    msg_sendEscapeMessage('Unsupported authentication type: ' + %char(type));
  endif;
  
  clear QUSEC;
  QUSBPRV = %size(QUSEC);
  system_getProfileHandle(basicAuthDs.username : basicAuthDs.password : handle :
    QUSEC : %len(%trimr(basicAuthDs.password)) : 0);
  if (QUSBAVL = 0);
    system_releaseProfileHandle(handle);
    return *on;
  else;
    return *off;
  endif;
end-proc;

dcl-proc droplet_auth_system_finalize export;
  dcl-pi *n;
    authenticator pointer;
  end-pi;
  
  dcl-ds authDs likeds(authenticator_t) based(authenticator);
  
  if (authDs.userdata <> *null);
    dealloc authDs.userdata;
  endif;
  
  dealloc(n) authenticator;
end-proc;

