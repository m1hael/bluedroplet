**FREE

/if not defined (BLUEDROPLET_ENDPOINT_H)
/define BLUEDROPLET_ENDPOINT_H

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------

dcl-ds endpoint_t qualified template;
  endPoint pointer(*proc);
  path char(1024);
  method char(10);
end-ds;

dcl-pr droplet_endpoint_getPath char(1024) extproc(*dclcase);
  endpoint pointer const;
end-pr;

dcl-pr droplet_endpoint_getMethod char(10) extproc(*dclcase);
  endpoint pointer const;
end-pr;

/endif
