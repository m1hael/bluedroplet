     /*********************************************************************/
     /*                                                                   */
     /* Header File Name: qlicvttp_h.rpgle                                */
     /* Definitions for use of API QLICVTTP                               */
     /* converts object Type to interal Hex representation                */
     /*                                                                   */
     /*********************************************************************/
      /IF NOT DEFINED (QLICVTTP_QRPGLEH)
      /DEFINE QLICVTTP_QRPGLEH
      /COPY 'errords.rpgle'
     /*===================================================================*/
     D ConvertObjectTypeHex...
     D                 PR                  EXTPGM('QLICVTTP')
     /* type of convert                                                   */
     /* *SYMTOHEX from Text to hex                                        */
     /* *HEXTOSYM from hex to text                                        */
     D ConversionType                10A   CONST
     /* type of object e.g. *FILE *SRVPGM                                 */
     D ObjectType                    10A
     /* type of object internal hex representation for APIs               */
     D HexType                        2A
     D Error                               LIKE(ErrorDS)
     D                                     OPTIONS(*OMIT:*VARSIZE)
     D                                     NOOPT
      /ENDIF

