      /IF NOT DEFINED (QLEAWI_QRPGLEH)
      /DEFINE QLEAWI_QRPGLEH
      /COPY 'errords.rpgle'
     /*********************************************************************/
     /* Header File Name: qleawi_h.rpgle                                  */
     /*  Prototypen aus QLEAWI.H in QSYSINC nach RPG transformiert        */
     /*  QleActBndPgm  dynamisches aktivieren Module zur Laufzeit         */
     /*  QleGetExp     Get Procedure Pointer und Exporte nach Name        */
     /*********************************************************************/
     /*  QleActBndPgm flags  0 war nicht aktiv                            */
      * war bereits aktiv
     D QLE_ABP_WAS_ACTIVE...
     D                 C                   x'80'
     /*-------------------------------------------------------------------*/
     /*  QleGetExport constants                                           */
     D QLE_EX_NOT_FOUND...
     D                 C                   0
     D QLE_EX_PROC     C                   1
     D QLE_EX_DATA     C                   2
     D QLE_EX_NO_ACCESS...
     D                 C                   3
     /*-------------------------------------------------------------------*/
     D Qle_ABP_Info_t  DS
      * Bytes returned (in/out)
     D Bytes_returned                10i 0
      * Bytes available (output)
     D Bytes_available...
     D                               10i 0
      * set to binary 0
     D                                8a
      * Activation Group Mark
     D Act_Grp_Mark                  10i 0
      * Activation Mark
     D Act_Mark                      10i 0
      * set to binary 0
     D                                7a
      * Flags siehe const Definitionen
     D ABPFlags                       1a
      * set to binary 0
     D                                3a
     /*-------------------------------------------------------------------*/
     D ActivateProgram...
     D                 PR            10i 0 EXTPROC('QleActBndPgm')
     D* Deklaration schwach dokumentiert
      * Pointer to Serviceprogram
     D                                 *   PROCPTR
     D                                     CONST
      * Activation mark
     D                               10i 0
      * Activation Information
     D Info                                LIKE(Qle_ABP_Info_t)
      * length of Activation Info
     D                               10i 0
      * Error code
     D Error                               LIKE(ErrorDS)
     D                                     OPTIONS(*OMIT:*VARSIZE)
     D                                     NOOPT
     /*-------------------------------------------------------------------*/
     D GetExport       PR                  EXTPROC('QleGetExp')
     /* Service pgm activation mark  */
     D                               10i 0
     /* Export Id                    */
     D                               10i 0 CONST
     /* Export name length           */
     D                               10i 0 CONST
     /* Export name                  */
     D                              256a   OPTIONS(*VARSIZE)
     D                                     CONST
     /* Pointer to Pointer to export */
     D                                 *   PROCPTR
     D                                     CONST
     /* type, see QLE_EX_* defn's    */
     D                               10i 0
     /* Error code                   */
     D Error                               LIKE(ErrorDS)
     D                                     OPTIONS(*OMIT:*VARSIZE)
     D                                     NOOPT
      /ENDIF

