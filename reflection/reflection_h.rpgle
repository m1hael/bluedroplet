      /IF NOT DEFINED (PROCP4NAME_QRPGLEH)
      /DEFINE PROCP4NAME_QRPGLEH
     /*********************************************************************/
     /* Header File Name: procedure4name_h.rpgle                          */
     /*  Reflection Program for ILE RPG                                   */
     /*  gets Serviceprogramm and Procedure Name                          */
     /*  returns Procedure Pointer                                        */
     /*  purpose: dynamic call of bindable procedures                     */
     /*  look also at program example TSTREFLECT.QRPGLESRC                */
     /*********************************************************************/
     D reflection_getProcedurePointer...
     D                 PR              *   procptr
     D                                     extproc('reflection_getProcedure-
     D                                     Pointer')
     D  serviceprogram...
     D                               10A   const
     D  procedure                   256A   const
      /ENDIF

