     /*********************************************************************/
     /*                                                                   */
     /* Header File Name: resolvePointer_h.rpgle                      */
     /* MI Function Resolve System Pointer                                */
     /*                                                                   */
     /*********************************************************************/
      /IF NOT DEFINED (RSLVSP_QRPGLEH)
      /DEFINE RSLVSP_QRPGLEH
      /COPY 'auth_const.rpgle'
     /*===================================================================*/
     D GetSysPointer   PR              *   PROCPTR
     D                                     EXTPROC('rslvsp')
     /* return value as Procedure Pointer weak documentation
     D HexType                        2A   VALUE
     D Object                          *   value options(*string)
     D Library                         *   value options(*string)
     D Authoritie                     2A   value
     /* constants declared in QRPGLEH.AUTH
      /ENDIF

