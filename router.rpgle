**FREE

///
// \brief BlueDroplet : Router Interface
//
// This module forwards the routing requests to the specific router 
// implementation.
//
// \author Mihael Schmidt
// \date   19.11.2016
//
// \interface
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2017 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'router_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'router_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc droplet_router_route export; 
  dcl-pi *N pointer;
    router pointer const;
    endPoints pointer const;
    method char(8) const;
    path char(1024) const;
  end-pi;

  dcl-ds routerDs likeds(droplet_router_t) based(router);
  dcl-s routePointer pointer(*proc);
  dcl-pr route pointer extproc(routePointer);
    router pointer const;
    endPoints pointer const;
    method char(8) const;
    path char(1024) const;
  end-pr;
  
  if (router = *null);
    return *null;
  endif;
  
  routePointer = routerDs.proc_route;
  
  return route(router : endPoints : method : path);
end-proc;



dcl-proc droplet_router_finalize export;
  dcl-pi *N;
    router pointer;
  end-pi;
  
  dcl-ds routerDs likeds(droplet_router_t) based(router);
  dcl-s finalizePointer pointer(*proc);
  dcl-pr finalize extproc(finalizePointer);
    router pointer;
  end-pr;
  
  if (router = *null);
    return;
  endif;
  
  finalizePointer = routerDs.proc_finalize;
  
  finalize(router);
  
  if (router <> *null);
    dealloc(n) router;
  endif;
end-proc;
