**FREE

///
// \brief BlueDroplet : HTTP Message Access Layer
//
// This module contains procedures to access the Mongoose HTTP Message
// data structure which is passed to the event handlers and REST end points.
//
// \author Mihael Schmidt
// \date   28.11.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'http_h.rpgle'
/include 'mongoose_h.rpgle'
/include 'llist/llist_h.rpgle'
/include 'libc_h.rpgle'
/include 'util_h.rpgle'
/include 'uriencode/uriencode_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc droplet_http_getMethod export;
  dcl-pi *N char(10);
    httpMessagePtr pointer const;
  end-pi;

  dcl-s method char(10);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  if (httpMessage.method.length > 0);
    method = %str(httpMessage.method.value : httpMessage.method.length);
  endif;

  return method;
end-proc;

dcl-proc droplet_http_getUri export;
  dcl-pi *N char(1024);
    httpMessagePtr pointer const;
  end-pi;

  dcl-s uri char(1024);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  if (httpMessage.uri.length > 0);
    uri = %str(httpMessage.uri.value : httpMessage.uri.length);
  endif;

  return uri;
end-proc;

dcl-proc droplet_http_getProtocol export;
  dcl-pi *N char(10);
    httpMessagePtr pointer const;
  end-pi;

  dcl-s protocol char(10);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  if (httpMessage.protocol.length > 0);
    protocol = %str(httpMessage.protocol.value : httpMessage.protocol.length);
  endif;

  return protocol;
end-proc;

dcl-proc droplet_http_getResponseCode export;
  dcl-pi *N int(10);
    httpMessagePtr pointer const;
  end-pi;

  dcl-s responseCode int(10);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  responseCode = httpMessage.responseCode;

  return responseCode;
end-proc;

dcl-proc droplet_http_getResponseStatusMessage export;
  dcl-pi *N varchar(32000);
    httpMessagePtr pointer const;
  end-pi;

  dcl-s statusMessage varchar(32000);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  if (httpMessage.responseStatusMessage.length > 0);
    statusMessage = %str(httpMessage.responseStatusMessage.value : httpMessage.responseStatusMessage.length);
  endif;

  return statusMessage;
end-proc;

dcl-proc droplet_http_getQueryString export;
  dcl-pi *N likeds(droplet_buffer);
    httpMessagePtr pointer const;
  end-pi;

  dcl-ds query likeds(droplet_buffer);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  query.string = httpMessage.queryString.value;
  query.length = httpMessage.queryString.length;

  return query;
end-proc;

dcl-proc droplet_http_getHeader export;
  dcl-pi *N char(1024);
    httpMessagePtr pointer const;
    headerName char(100) const;
  end-pi;

  dcl-s i int(10);
  dcl-s passedHeaderName char(100);
  dcl-s passedHeaderLength int(10);
  dcl-s headerValue char(1024);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  passedHeaderName = headerName;
  passedHeaderLength = %len(%trimr(headerName));

  for i = 1 to MONGOOSE_MAX_HTTP_HEADERS;
    if (httpMessage.headerNames(i).length = passedHeaderLength);

      if (memcmp(%addr(passedHeaderName) : httpMessage.headerNames(i).value : passedHeaderLength) = 0);
        headerValue = %str(httpMessage.headerValues(i).value : httpMessage.headerValues(i).length);
        leave;
      endif;

    endif;
  endfor;

  return headerValue;
end-proc;

dcl-proc droplet_http_getHeaderNames export;
  dcl-pi *N pointer;
    httpMessagePtr pointer const;
  end-pi;

  dcl-s i int(10);
  dcl-s names pointer;
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  names = list_create();

  for i = 1 to MONGOOSE_MAX_HTTP_HEADERS;
    if (httpMessage.headerNames(i).length > 0);
      list_add(names : httpMessage.headerNames(i).value : httpMessage.headerNames(i).length);
    endif;
  endfor;

  return names;
end-proc;

dcl-proc droplet_http_getBody export;
  dcl-pi *N likeds(droplet_buffer);
    httpMessagePtr pointer const;
  end-pi;

  dcl-ds body likeds(droplet_buffer);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);

  clear body;

  if (httpMessage.body.length > 0);
    body.string = httpMessage.body.value;
    body.length = httpMessage.body.length;
  endif;

  return body;
end-proc;

dcl-proc droplet_http_getQueryValue export;
  dcl-pi *n char(1024);
    httpMessagePtr pointer const;
    key char(1024) const;
  end-pi;

  dcl-s abnormallyEnded ind;
  dcl-s value char(1024);
  dcl-s values pointer;
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);
  dcl-ds queryPart likeds(http_queryPart_t) based(queryPartPtr);

  if (httpMessage.queryString.length = 0);
    return value;
  endif;

  values = droplet_http_getQueryValues(httpMessagePtr);

  queryPartPtr = list_iterate(values);
  dow (queryPartPtr <> *null);
    if (queryPart.key = key);
      value = queryPart.value;
      leave;
    endif;

    queryPartPtr = list_iterate(values);
  enddo;

  return value;

  on-exit abnormallyEnded;
    if (values <> *null);
      list_dispose(values);
    endif;
end-proc;


dcl-proc droplet_http_getQueryValues export;
  dcl-pi *n pointer;
    httpMessagePtr pointer const;
  end-pi;

  dcl-s abnormallyEnded ind;
  dcl-s values pointer;
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);
  dcl-s queryString char(100000) based(queryStringPtr);
  dcl-s numberDecodedChars int(10);

  values = list_create();

  if (httpMessage.queryString.length = 0);
    return values;
  endif;

  queryStringPtr = %alloc(httpMessage.queryString.length);

  numberDecodedChars = decodeUri(httpMessage.queryString.value :
                           httpMessage.queryString.length : queryStringPtr);
  local_parseQueryString(values : queryStringPtr : numberDecodedChars);

  return values;

  on-exit abnormallyEnded;
    if (queryStringPtr <> *null);
      dealloc queryStringPtr;
    endif;
end-proc;


dcl-proc droplet_http_containsQueryKey export;
  dcl-pi *n ind;
    httpMessagePtr pointer const;
    key char(1024) const;
  end-pi;

  dcl-s abnormallyEnded ind;
  dcl-s exists ind inz(*off);
  dcl-s values pointer;
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);
  dcl-ds queryPart likeds(http_queryPart_t) based(queryPartPtr);

  if (httpMessage.queryString.length = 0);
    return exists;
  endif;

  values = droplet_http_getQueryValues(httpMessagePtr);

  queryPartPtr = list_iterate(values);
  dow (queryPartPtr <> *null);
    if (queryPart.key = key);
      exists = *on;
      leave;
    endif;

    queryPartPtr = list_iterate(values);
  enddo;

  return exists;

  on-exit abnormallyEnded;
    if (values <> *null);
      list_dispose(values);
    endif;
end-proc;


dcl-proc droplet_http_getPathElement export;
  dcl-pi *n char(1024);
    httpMessagePtr pointer const;
    index uns(10) const;
  end-pi;

  dcl-s element char(1024);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(httpMessagePtr);
  dcl-s uriChar char(1) based(ptr);
  dcl-s uriLength int(10);
  dcl-s currentElementIndex int(10);
  dcl-s i int(10);
  dcl-s x int(10);
  dcl-s start pointer;
  dcl-s end pointer;

  dcl-ds *n;
    ci uns(3) inz(0);
    cc char(1) overlay(ci) inz('/');
  end-ds;

  if (index = 0);
    return element;
  endif;

  dow (i < httpMessage.uri.length);
    ptr = httpMessage.uri.value + i;

    if (uriChar = '/');
      currentElementIndex += 1;

      if (currentElementIndex = index);
        // this is the correct element

        if (i < httpMessage.uri.length-1);
          start = ptr + 1;

          end = memchr(start : ci : httpMessage.uri.length - i - 1);
          if (end = *null);
            element = %str(start : httpMessage.uri.length - i - 1);
          else;
            element = %str(start : end - start);
          endif;

          leave;
        endif;

      endif;
    endif;

    i += 1;
  enddo;

  return element;
end-proc;


dcl-proc local_parseQueryString export;
  dcl-pi *n;
    values pointer const;
    queryString pointer const;
    length int(10) const;
  end-pi;

  dcl-s start pointer;
  dcl-s c char(1) based(ptr);
  dcl-s i int(10);
  dcl-ds queryPart likeds(http_queryPart_t) inz;
  dcl-s lastSeparator char(1);

  // note: the query string does not contain the fragment.
  //       the fragment has been excluded by the http server.
  //       it also does _not_start with ?

  if (length = 0);
    return;
  endif;

  start = queryString;

  for i = 1 to length;
    ptr = queryString + (i - 1);

    if (c = '=');
      clear queryPart;

      if (start = ptr);
        // empty key: query string: =value   ... no key
      else;
        queryPart.key = %str(start : ptr - start );
      endif;

      start = ptr + 1;

      lastSeparator = '=';
    elseif (c = '&');
      if (start = ptr);

        if (lastSeparator = '&');
          // totally empty => do nothing
        elseif (lastSeparator = '=');
          list_add(values : %addr(queryPart) : %size(querypart));
          clear queryPart;
        endif;

      else;

        if (lastSeparator = '=');
          queryPart.value = %str(start : ptr - start);
          list_add(values : %addr(queryPart) : %size(querypart));
          clear queryPart;
        elseif (lastSeparator = '&');
          list_add(values : %addr(queryPart) : %size(querypart));

          clear queryPart;
          queryPart.key = %str(start : ptr - start );
        else;
          queryPart.key = %str(start : ptr - start );
          list_add(values : %addr(queryPart) : %size(querypart));
          clear queryPart;
        endif;

      endif;

      start = ptr + 1;

      lastSeparator = '&';
    endif;
  endfor;

  if (lastSeparator = *blank);
    queryPart.key = %str(start : ptr - start + 1);
    list_add(values : %addr(queryPart) : %size(querypart));
  elseif (lastSeparator = '=');
    if (start-1 <> ptr);
      queryPart.value = %str(start : ptr - start + 1);
    endif;
    list_add(values : %addr(queryPart) : %size(querypart));
  elseif (lastSeparator = '&');
    if (start-1 <> ptr);
      queryPart.key = %str(start : ptr - start + 1);
      list_add(values : %addr(queryPart) : %size(querypart));
    endif;
  endif;

end-proc;


///
// Decode a percent encoded URI
//
// Decodes a percent encoded string. The string is expected to be EBCDIC encoded
// in the local encoding.
// <br><br>
// The memory of the encoded string will not be changed in the process.
//
// @param Pointer to encoded string
// @param Length of encoded string
// @param Pointer to space for the decoded string
// @return Length of the decoded string
///
dcl-proc decodeUri;
  dcl-pi *n int(10);
    encoded pointer const;
    length int(10) const;
    decoded pointer const;
  end-pi;

  dcl-s c char(1) based(cptr);
  dcl-s c2 char(2) based(cptr);
  dcl-s c5 char(5) based(cptr);
  dcl-s c8 char(8) based(cptr);
  dcl-s c11 char(11) based(cptr);
  dcl-s ca3 char(12) ccsid(*utf8);
  dcl-s receiver char(4) ccsid(*utf8);
  dcl-s i int(10);
  dcl-s decodedChar char(1) based(dptr);
  dcl-s numberDecodedChars int(10);

  cptr = encoded;
  dptr = decoded;
  dow (i < length);
    if (c = '%' and i+2 < length);
      cptr += 1;

      receiver = *blank;

      // TODO refactor this to make it faster if the character is a single byte unicode
      //      as this will be the case most of the time

      // Unicode:
      // 1 byte = < %A0
      // 2 byte = %C0 - %DF
      // 3 byte = %E0 - %EF
      // 4 byte = %F0 - %FF
      if (c = 'c' or c = 'C' or c = 'd' or c = 'D');
        // 2 byte unicode character
        ca3 = '%' + c5;
        uri_decode(%addr(ca3) : 6 : %addr(receiver));
        decodedChar = receiver;
        cptr += 5;
        i+= 5;
      elseif (c = 'e' or c = 'E');
        // 3 byte unicode character

        // TODO

      elseif (c = 'e' or c = 'E');
        // 4 byte unicode character

        // TODO

      else;
        // single byte unicode character
        ca3 = '%' + c2;
        uri_decode(%addr(ca3) : 3 : %addr(receiver));
        decodedChar = receiver;
        cptr += 2;
        i+= 2;
      endif;

      numberDecodedChars += 1;
      dptr += 1;

      i += 1;

    else;
      decodedChar = c;
      cptr += 1;

      numberDecodedChars += 1;
      dptr += 1;

      i += 1;
    endif;

  enddo;

  return numberDecodedChars;
end-proc;
