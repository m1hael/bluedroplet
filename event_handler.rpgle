**FREE

///
// BlueDroplet : Mongoose Main Event Handler
//
// This event handler receives every request and proxies them to the correct
// user event handler (REST service end point).
//
// As this is the first place where the data arrives it will be converted here
// to the correct CCSID if the flags for auto encoding are set.
//
// \author Mihael Schmidt
// \date   15.11.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'service_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'endpoint_h.rpgle'
/include 'mongoose_h.rpgle'
/include 'service_h.rpgle'
/include 'http_h.rpgle'
/include 'mime_h.rpgle'
/include 'util_h.rpgle'
/include 'router_h.rpgle'
/include 'logger_h.rpgle'
/include 'iconv_h.rpgle'
/include 'libc_h.rpgle'
/include 'message_h.rpgle'
/include 'filter_h.rpgle'


dcl-pr handleEvents extproc('handleEvents');
  connection pointer value;
  event int(10) value;
  userData pointer value;
end-pr;

dcl-pr filterRequest ind extproc('filterRequest');
  service pointer const;
  connection pointer const;
  httpMessage pointer const;
end-pr;

dcl-pr getMessageBodyIndex int(10);
  buffer likeds(mongoose_buffer) const;
end-pr;

dcl-pr getMessageBodyEncoding int(10);
  service pointer const;
  buffer likeds(mongoose_buffer) const;
  messageBodyIndex int(10);
end-pr;

dcl-pr encodeMessageHeader;
  service pointer const;
  buffer likeds(mongoose_buffer) const;
  messageBodyIndex int(10) const;
end-pr;

dcl-pr encodeMessageBody pointer;
  service pointer const;
  body likeds(mongoose_buffer);
  ccsid int(10);
end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc handleEvents export;
  dcl-pi *n;
    connection pointer value;
    event int(10) value;
    userData pointer value;
  end-pi;

  dcl-ds connectionDs likeds(mongoose_connection) based(connection);
  dcl-ds serviceDs likeds(service_t) based(connectionDs.userData);
  dcl-ds endPointDs likeds(endpoint_t) based(endPoint);
  dcl-ds httpMessage likeds(mongoose_httpMessage) based(userData);
  dcl-s httpMethod char(8);
  dcl-s uri char(1024);
  dcl-s endPoint pointer;
  dcl-s messageBodyIndex int(10);
  dcl-s messageBodyEncoding int(10);
  dcl-s encodedMessageBody pointer;

  dcl-pr user_handleRequest extproc(endPointDs.endPoint);
    service pointer const;
    connection pointer const;
    httpMessage pointer const;
    endPoint pointer const;
  end-pr;

  if (event = MONGOOSE_EVENT_ACCEPT);
    // initialize
    droplet_logger_debug(serviceDs.logger : 'Connection created.');
  elseif (event = MONGOOSE_EVENT_RECEIVED);
    droplet_logger_debug(serviceDs.logger : %char(connectionDs.receivingBuffer.length) +
                         ' bytes received from client.');

    messageBodyIndex = getMessageBodyIndex(connectionDs.receivingBuffer);

    encodeMessageHeader(connectionDs.userData : connectionDs.receivingBuffer : messageBodyIndex);

    if (messageBodyIndex = -1);
      // no message body separator => no message body
    elseif (messageBodyIndex = connectionDs.receivingBuffer.length);
      // message body length of zero => no message body
    elseif (serviceDs.autoEncodeInput = *on);
      droplet_logger_debug(serviceDs.logger : 'Message body length: ' + %char(httpMessage.body.length));

      messageBodyEncoding = getMessageBodyEncoding(connectionDs.userData : connectionDs.receivingBuffer : messageBodyIndex);
      if (messageBodyEncoding = 0);
        messageBodyEncoding = 819;
      endif;

      encodedMessageBody = encodeMessageBody(connectionDs.userData : httpMessage.body : messageBodyEncoding);

      if (encodedMessageBody <> *null);
        // resize buffer to just message header
        connectionDs.receivingBuffer.length = messageBodyIndex;

        // append encoded body to buffer
        mongoose_buffer_append(%addr(connectionDs.receivingBuffer) : encodedMessageBody : strlen(encodedMessageBody));

        dealloc encodedMessageBody;
      endif;
    else;
      //leave it be, no encoding
    endif;

  elseif (event = MONGOOSE_EVENT_HTTP_REQUEST);
    droplet_logger_debug(serviceDs.logger : 'HTTP request received.');

    monitor;
      httpMethod = %str(httpMessage.method.value : httpMessage.method.length);
      uri = %str(httpMessage.uri.value : httpMessage.uri.length);

      endPoint = droplet_router_route(serviceDs.router : serviceDs.endPoints : httpMethod : uri);

      if (endPoint = *null);
        // no matching end point => return HTTP 404 NOT FOUND
        droplet_logger_debug(serviceDs.logger : 'No matching end point for HTTP request to URI ' + %trimr(uri));
        droplet_service_notFound(connection);
      else;
        droplet_logger_debug(serviceDs.logger : 'Routing HTTP request to end point ' + %trimr(endPointDs.path));

        if (filterRequest(connectionDs.userData : connection : userData));
          user_handleRequest(connectionDs.userData : connection : userData : endPoint);
        endif;
      endif;
    on-error *all;
      // TODO get the message text from the last escape message: message_receiveMessageText();
      droplet_service_serverError(connection);
    endmon;
  elseif (event = MONGOOSE_EVENT_CLOSE);
    // cleanup
    droplet_logger_debug(serviceDs.logger : 'Connection closed.');
  endif;

end-proc;

dcl-proc getMessageBodyIndex;
  dcl-pi *N int(10);
    buffer likeds(mongoose_buffer) const;
  end-pi;

  dcl-s x int(10) inz(4);
  dcl-s ptr pointer;
  dcl-s bufferRange char(4) based(ptr);

  ptr = buffer.buffer;
  dow (x <= buffer.length);
    if (bufferRange = x'0D0A0D0A');
      return x;
    endif;

    ptr = ptr + 1;
    x += 1;
  enddo;

  return -1;
end-proc;

///
// Encode HTTP Message Header
//
// The HTTP Message Header should always be in ASCII. So we need to convert the
// header from 819 to the local CCSID (see service data structure).
//
// As (SBCS) EBCDIC is a one byte character set same as ASCII we can convert the header
// in place, using the same buffer for input and output.
//
// \param Pointer to service data structure
// \param Mongoose receiving data buffer
// \param Index of the HTTP message body (may be -1 for no message body)
///
dcl-proc encodeMessageHeader;
  dcl-pi *N;
    service pointer const;
    buffer likeds(mongoose_buffer) const;
    messageBodyIndex int(10) const;
  end-pi;

  dcl-ds serviceDs likeds(service_t) based(service);
  dcl-s messageHeaderLength uns(10);
  dcl-s inBuffer pointer;
  dcl-s outBuffer pointer;
  dcl-s returnValue int(10);
  dcl-s error int(10) based(errorPtr);

  if (messageBodyIndex = -1);
    messageHeaderLength = buffer.length;
  else;
    messageHeaderLength = messageBodyIndex; // also encode the message body separator
  endif;

  // need to convert to EBCDIC (also need to use different
  // pointers as iconv will change the value of the pointer)
  inBuffer = buffer.buffer;
  outBuffer = buffer.buffer;

  returnValue = iconv(serviceDs.iconvFromAscii :
                      inBuffer : messageHeaderLength :
                      outBuffer : messageHeaderLength);
  if (returnValue = -1);
    errorPtr = errno();
    droplet_logger_error(serviceDs.logger : 'Could not convert message header to EBCDIC. ' +
                        %char(error) + ' - ' + %str(strerror(error)));
  endif;
end-proc;

dcl-proc encodeMessageBody;
  dcl-pi *N pointer;
    service pointer const;
    body likeds(mongoose_buffer);
    ccsid int(10);
  end-pi;

  dcl-ds serviceDs likeds(service_t) based(service);
  dcl-s output pointer;
  dcl-s inBuffer pointer;
  dcl-s inBufferLength uns(10);
  dcl-s outBuffer pointer;
  dcl-s outBufferLength uns(10);
  dcl-s returnValue int(10);
  dcl-s error int(10) based(errorPtr);
  dcl-s encodedMessage pointer;
  dcl-ds iconvDesc likeds(iconv_t);

  droplet_logger_debug(serviceDs.logger : 'Using CCSID ' + %char(ccsid) + ' for encoding the message body.');

  inBuffer = body.buffer;
  inBufferLength = body.length;
  outBufferLength = body.length * 4 + 1;
  output = %alloc(outBufferLength);
  outBuffer = output; // need an extra pointer because iconv will change this pointer
  memset(outBuffer : x'00' : outBufferLength);

  iconvDesc = droplet_util_createConversionDescriptor(ccsid : 0);

  returnValue = iconv(iconvDesc : inBuffer : inBufferLength : outBuffer : outBufferLength);
  if (returnValue = -1);
    errorPtr = errno();
    droplet_logger_error(serviceDs.logger : 'Could not convert message body to EBCDIC. ' +
                        %char(error) + ' - ' + %str(strerror(error)));
    dealloc(ne) output;
    return *null;
  else;
    return output;
  endif;
end-proc;

///
// Get CCSID for Message Body
//
// Returns the CCSID how the message body is encoded.
//
// \param Pointer to service
// \param Receiving Mongoose Buffer
// \param Index where the message body starts in the HTTP message
//
// \return CCSID for the message body or 0 if no CCSID could be resolved
///
dcl-proc getMessageBodyEncoding;
  dcl-pi *N int(10);
    service pointer const;
    buffer likeds(mongoose_buffer) const;
    messageBodyIndex int(10);
  end-pi;

  dcl-ds serviceDs likeds(service_t) based(service);
  dcl-s headerEntry char(100) based(ptr);
  dcl-s headerLength int(10);
  dcl-s charset char(50);
  dcl-s contentTypeHeader char(13);
  contentTypeHeader = 'Content-Type' + x'00';

  // search for the HTTP header Content-Type
  ptr = strstr(buffer.buffer : %addr(contentTypeHeader));
  if (ptr = *NULL);
    droplet_logger_debug(serviceDs.logger : 'No Content-Type HTTP header present.');
  else;
    // HTTP header Content-Type is part of the message => check for charset
    headerLength = %scan(x'0D25' : headerEntry);
    if (headerLength = 0);
      droplet_logger_debug(serviceDs.logger : 'Content-Type header too long (more than 100 characters).');
    else;

      charset = droplet_util_getCharsetFromHttpHeader(%subst(headerEntry : 1 : headerLength));
      if (charset = *blank);
        droplet_logger_debug(serviceDs.logger : 'No charset specified.');
      else;
        droplet_logger_debug(serviceDs.logger : 'Message body encoded with charset ' + %trimr(charset));
        return droplet_mime_getCcsid(charset);
      endif;

    endif;
  endif;

  return 0;
end-proc;


///
// Filter request
//
// Filters the request with the registered filters.
//
// @param Service
// @param Connection
// @param HTTP message
// @return *on if the request passed the filter, *off if the request should be blocked
//
// @info Any registered filter may send a HTTP response. If a response is send the
//       request will be blocked (and also the other way round: If the request is
//       blocked the filter who blocked the request sends a HTTP response).
///
dcl-proc filterRequest export;
  dcl-pi *n ind;
    service pointer const;
    connection pointer const;
    httpMessage pointer const;
  end-pi;
  
  dcl-ds serviceDs likeds(service_t) based(service);
  
  if (serviceDs.filter = *null);
    return *on;
  endif;
  
  return droplet_filter_filterRequest(serviceDs.filter : service : connection : httpMessage);
end-proc;

