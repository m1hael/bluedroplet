**FREE

///
// \brief BlueDroplet : Authentication Interface
//
// This module implements an interface for authentication. The authentication
// request is passed to the implementation.
//
// \author Mihael Schmidt
// \date   28.08.2018
//
///

//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

ctl-opt nomain;


//
// Prototypes
//
/include 'auth_h.rpgle'


//
// Procedures
//
dcl-proc droplet_auth_authenticate export;
  dcl-pi *n ind;
    authenticator pointer const;
    credentials pointer const;
    type int(5) const;
  end-pi;
  
  dcl-ds authDs likeds(authenticator_t) based(authenticator);
  dcl-s authenticatePointer pointer(*proc);
  dcl-pr authenticate ind extproc(authenticatePointer);
    authenticator pointer const;
    credentials pointer const;
    type int(5) const;
  end-pr;
  
  authenticatePointer = authDs.proc_authenticate;
  
  return authenticate(authenticator : credentials : type);
end-proc;

dcl-proc droplet_auth_finalize export;
  dcl-pi *n;
    authenticator pointer;
  end-pi;
  
  dcl-ds authDs likeds(authenticator_t) based(authenticator);
  dcl-s finalizePointer pointer(*proc);
  dcl-pr finalize extproc(finalizePointer);
    authenticator pointer;
  end-pr;
  
  if (authenticator = *null);
    return;
  endif;
  
  finalizePointer = authDs.proc_finalize;
  
  finalize(authenticator);
  
  if (authenticator <> *null);
    dealloc(n) authenticator;
  endif;
end-proc;
