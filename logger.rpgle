**FREE

///
// \brief BlueDroplet : Logger Interface
//
// \author Mihael Schmidt
// \date   18.11.2016
//
// \interface
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'logger_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'logger_h.rpgle'
/include 'reflection/reflection_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc droplet_logger_create export;
  dcl-pi *N pointer;
    serviceprogram char(10) const;
    createProcedure char(256) const;
    userData pointer const;
  end-pi;
  
  dcl-s logger pointer;
  dcl-s procedurePointer pointer(*proc);
  dcl-pr create pointer extproc(procedurePointer);
    userdata pointer const;
  end-pr;
  
  procedurePointer = reflection_getProcedurePointer(serviceProgram : %trimr(createProcedure));

  if (procedurePointer <> *null);
    logger = create(userData);
  endif;

  return logger;
end-proc;

dcl-proc droplet_logger_debug export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  dcl-ds loggerDs likeds(logger_t) based(logger);
  dcl-s debugPointer pointer(*proc);
  dcl-pr debug extproc(debugPointer);
    logger pointer const;
    message varchar(1024) const;
  end-pr;
  
  if (logger = *null);
    return;
  endif;
  
  // check log level: info, warn, error = not ok
  if (loggerDs.logLevel <> DROPLET_LOG_LEVEL_DEBUG);
    return;
  endif;
  
  debugPointer = loggerDs.proc_log_debug;
  
  debug(logger : message);
end-proc;


dcl-proc droplet_logger_info export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  dcl-ds loggerDs likeds(logger_t) based(logger);
  dcl-s infoPointer pointer(*proc);
  dcl-pr info extproc(infoPointer);
    logger pointer const;
    message varchar(1024) const;
  end-pr;
  
  if (logger = *null);
    return;
  endif;
  
  // check log level: debug, info = ok
  if (loggerDs.logLevel = DROPLET_LOG_LEVEL_WARN or 
      loggerDs.logLevel = DROPLET_LOG_LEVEL_ERROR);
    return;
  endif;
  
  infoPointer = loggerDs.proc_log_info;
  
  info(logger : message);
end-proc;


dcl-proc droplet_logger_warn export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  dcl-ds loggerDs likeds(logger_t) based(logger);
  dcl-s warnPointer pointer(*proc);
  dcl-pr warn extproc(warnPointer);
    logger pointer const;
    message varchar(1024) const;
  end-pr;
  
  if (logger = *null);
    return;
  endif;
  
  // check log level: debug, info, warn = ok
  if (loggerDs.logLevel = DROPLET_LOG_LEVEL_ERROR);
    return;
  endif;
  
  warnPointer = loggerDs.proc_log_warn;
  
  warn(logger : message);
end-proc;


dcl-proc droplet_logger_error export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  dcl-ds loggerDs likeds(logger_t) based(logger);
  dcl-s errorPointer pointer(*proc);
  dcl-pr error extproc(errorPointer);
    logger pointer const;
    message varchar(1024) const;
  end-pr;
  
  if (logger = *null);
    return;
  endif;
  
  errorPointer = loggerDs.proc_log_error;
  
  error(logger : message);
end-proc;


dcl-proc droplet_logger_finalize export;
  dcl-pi *N;
    logger pointer;
  end-pi;
  
  dcl-ds loggerDs likeds(logger_t) based(logger);
  dcl-s finalizePointer pointer(*proc);
  dcl-pr finalize extproc(finalizePointer);
    logger pointer;
  end-pr;
  
  if (logger = *null);
    return;
  endif;
  
  finalizePointer = loggerDs.proc_finalize;
  
  finalize(logger);
  
  if (logger <> *null);
    dealloc(n) logger;
  endif;
end-proc;


dcl-proc droplet_logger_setLogLevel export;
  dcl-pi *N;
    logger pointer const;
    logLevel char(10) const;
  end-pi;
  
  dcl-ds loggerDs likeds(logger_t) based(logger);
  
  if (logger = *null);
    return;
  endif;
  
  loggerDs.logLevel = logLevel;
end-proc;

