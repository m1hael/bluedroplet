**FREE

///
// \brief BlueDroplet : Simple Logger Implementation
//
// This module implements the logger interface. It outputs the log messages
// via the opcode DSPLY prepended by the severity level.
//
// \author Mihael Schmidt
// \date   18.11.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'logger_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'simple_logger_h.rpgle'

dcl-pr log;
  prefix char(8) const;
  message varchar(1024) const;
end-pr;
  

//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc log;
  dcl-pi *N;
    prefix char(8) const;
    message varchar(1024) const;
  end-pi;
  
  dcl-s output char(50);
  
  output = prefix + message;
  dsply output;
end-proc;


dcl-proc droplet_logger_simple_create export;
  dcl-pi *N pointer;
    userData pointer const options(*nopass);
  end-pi;

  dcl-s logger pointer;
  dcl-ds loggerDs likeds(logger_t) based(logger);
  
  logger = %alloc(%size(logger_t));
  loggerDs.id = 'droplet_logger_simple';
  loggerDs.logLevel = DROPLET_LOG_LEVEL_INFO;
  loggerDs.proc_log_debug = %paddr('droplet_logger_simple_debug');
  loggerDs.proc_log_info = %paddr('droplet_logger_simple_info');
  loggerDs.proc_log_warn = %paddr('droplet_logger_simple_warn');
  loggerDs.proc_log_error = %paddr('droplet_logger_simple_error');
  loggerDs.proc_finalize = %paddr('droplet_logger_simple_finalize');
  
  return logger;
end-proc;


dcl-proc droplet_logger_simple_debug export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  log('[DEBUG]' : message);
end-proc;


dcl-proc droplet_logger_simple_info export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  log('[INFO]' : message);
end-proc;


dcl-proc droplet_logger_simple_warn export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  log('[WARN]' : message);
end-proc;


dcl-proc droplet_logger_simple_error export;
  dcl-pi *N;
    logger pointer const;
    message varchar(1024) const;
  end-pi;
  
  log('[ERROR]' : message);
end-proc;


dcl-proc droplet_logger_simple_finalize export;
  dcl-pi *N;
    logger pointer;
  end-pi;
  
  if (logger <> *null);
    dealloc(n) logger;
  endif;
  
end-proc;
