**FREE

/if not defined (BLUEDROPLET_ROUTER_T)
/define BLUEDROPLET_ROUTER_T

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Templates
//-------------------------------------------------------------------------------------------------
dcl-ds droplet_router_t qualified template;
  id char(50);
  userdata pointer;
  proc_route pointer(*proc);
  proc_finalize pointer(*proc);
end-ds;

/endif
