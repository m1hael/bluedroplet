**FREE

/if not defined (BLUEDROPLET_MIME_H)
/define BLUEDROPLET_MIME_H

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------

///
// Get CCSID for MIME charset
//
// Returns the corresponding CCSID to the passed MIME charset value.
//
// \param charset
// \return corresponding CCSID or 0 if there is no matching value
///
dcl-pr droplet_mime_getCcsid int(10) extproc('droplet_mime_getCcsid');
  charset char(50) const;
end-pr;

/endif
