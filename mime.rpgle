**FREE

///
// BlueDroplet : MIME Types and Charsets
//
// This module provides utility procedures for MIME types and charsets.
//
// \author Mihael Schmidt
// \date   19.12.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Global Variables
//-------------------------------------------------------------------------------------------------
// from http://webcheatsheet.com/html/character_sets_list.php
dcl-s charsets char(50) dim(150) ctdata;
// from http://help.dottoro.com/laiuuxpb.php
// and from IBM Globalization Coded character set identifiers
// http://www-01.ibm.com/software/globalization/ccsid/ccsid_registered.html
dcl-s ccsids int(10) dim(150) ctdata;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
/include 'mime_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc droplet_mime_getCcsid export;
  dcl-pi *N int(10);
    charset char(50) const;
  end-pi;
  
  dcl-c uppercase 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  dcl-c lowercase 'abcdefghijklmnopqrstuvwxyz';
  dcl-s value char(50);
  dcl-s index int(10);
  
  // we can do a simple lowercase via xlate because HTTP header entries are in ASCII
  value = %xlate(uppercase : lowercase : charset);
  index = %lookup(value : charsets);
  if (index = 0);
    return 0;
  else;
    return ccsids(index);
  endif;
end-proc;


**CTDATA charsets
asmo-708
dos-720
iso-8859-6
x-mac-arabic
windows-1256
ibm775
iso-8859-4
windows-1257
ibm852
iso-8859-2
x-mac-ce
windows-1250
euc-cn
gb2312
hz-gb-2312
x-mac-chinesesimp
big5
x-chinese-cns
x-chinese-eten
x-mac-chinesetrad
950
cp866
iso-8859-5
koi8-r
koi8-u
x-mac-cyrillic
windows-1251
x-europa
x-ia5-german
ibm737
iso-8859-7
x-mac-greek
windows-1253
ibm869
dos-862
iso-8859-8-i
iso-8859-8
x-mac-hebrew
windows-1255
x-ebcdic-arabic
x-ebcdic-cyrillicrussian
x-ebcdic-cyrillicserbianbulgarian
x-ebcdic-denmarknorway
x-ebcdic-denmarknorway-euro
x-ebcdic-finlandsweden
x-ebcdic-finlandsweden-euro
x-ebcdic-france
x-ebcdic-france-euro
x-ebcdic-germany
x-ebcdic-germany-euro
x-ebcdic-greekmodern
x-ebcdic-greek
x-ebcdic-hebrew
x-ebcdic-icelandic
x-ebcdic-icelandic-euro
x-ebcdic-international
x-ebcdic-international-euro
x-ebcdic-italy
x-ebcdic-italy-euro
x-ebcdic-japaneseandkana
x-ebcdic-japaneseandjapaneselatin
x-ebcdic-japaneseanduscanada
x-ebcdic-japanesekatakana
x-ebcdic-koreanandkoreanextended
x-ebcdic-koreanextended
cp870
x-ebcdic-simplifiedchinese
x-ebcdic-spain
x-ebcdic-spain-euro
x-ebcdic-thai
x-ebcdic-traditionalchinese
cp1026
x-ebcdic-turkish
x-ebcdic-uk
x-ebcdic-uk-euro
ebcdic-cp-us
x-ebcdic-cp-us-euro
ibm861
x-mac-icelandic
x-iscii-as
x-iscii-be
x-iscii-de
x-iscii-gu
x-iscii-ka
x-iscii-ma
x-iscii-or
x-iscii-pa
x-iscii-ta
x-iscii-te
euc-jp
x-euc-jp
iso-2022-jp
iso-2022-jp
csiso2022jp
x-mac-japanese
shift_jis
ks_c_5601-1987
euc-kr
iso-2022-kr
johab
x-mac-korean
iso-8859-3
iso-8859-15
x-ia5-norwegian
ibm437
x-ia5-swedish
windows-874
ibm857
iso-8859-9
x-mac-turkish
windows-1254
unicode
unicodefffe
utf-7
utf-8
us-ascii
windows-1258
ibm850
x-ia5
iso-8859-1
macintosh
windows-1252
**CTDATA ccsids
         0
       720
      1089
         0
      1256
       775
       914
      1257
       852
       912
         0
         0
         0
         0
         0
         0
       947
      5056
       950
       950
         0
       866
       915
      1167
      1168
         0
      1251
      1252
         0
       737
       813
         0
      1253
       869
       862
       916
       916
         0
      1255
       420
       880
      1025
       277
      1142
       278
      1143
       297
      1147
       273
      1141
       875
       423
       424
       871
      1149
       500
      1148
       280
      1144
         0
      9219
         0
       290
         0
         0
       870
         0
       284
      1145
       838
         0
      1026
       905
       285
      1146
        37
      1140
       861
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
         0
       970
         0
         0
         0
       913
       923
         0
       437
         0
      1162
       857
       920
      1281
       920
      1200
      1201
         0
      1208
       367
      1258
       850
         0
       819
         0
      1252
