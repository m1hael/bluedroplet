**FREE

///
// \brief Program Messages Utilities
//
// This service program simplifies the sending of program messages.
//
// \author Mihael Schmidt
// \date   10.03.2009
//
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'ceeapi_h.rpgle'
/include 'message_h.rpgle'

/if not defined(QUSEC)
/define QUSEC
/include QSYSINC/QRPGLESRC,QUSEC
/endif

dcl-pr sendProgramMessage extpgm('QMHSNDPM');
  szMsgID char(7) const;
  szMsgFile char(20) const;
  szMsgData char(6000) const options(*varsize);
  nMsgDataLen int(10) const;
  szMsgType char(10) const;
  szCallStkEntry char(10) const;
  nRelativeCallStkEntry int(10) const;
  szRtnMsgKey char(4);
  apiErrorDS likeds(QUSEC) options(*varsize);
end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------

///
// \brief Send program message
//
// This procedures sends a program message via the system API QMHSNDPM (Send Program Message).
//
// <br><br>
//
// By default the message will be sent one level above the current stack level.
//
// \param message
// \param message type (default: *INFO)
// \param stack level (default: 1)
///
dcl-proc msg_sendProgramMessage export;
  dcl-pi *N opdesc;
    message char(6000) const options(*varsize);
    messageType char(10) const options(*omit : *nopass);
    callStackDepth int(10) const options(*nopass);
  end-pi;

  //
  // Parameters passed to CEEDOD
  //
  dcl-s descType int(10);
  dcl-s dataType int(10);
  dcl-s descInfo1 int(10);
  dcl-s descInfo2 int(10);
  dcl-s length int(10);
 
  dcl-s msgType char(10) inz('*INFO');
  dcl-s msgLength int(10);
  dcl-s stacklevel int(10) inz(1);
  dcl-s msgkey char(4);
  dcl-ds errords likeds(QUSEC) inz;
  
  cee_getOpDescInfo( 1 : descType : dataType : descInfo1 : descInfo2 : length : *omit);
  
  msgLength = %len(%trimr(%subst(message : 1 : length)));
  
  if (%parms() >= 2 and %addr(messageType) <> *null);
    msgType = messageType;
  endif;
  
  if (%parms() = 3);
    stacklevel = callStackDepth;
  endif;
  
  sendProgramMessage(*blank :
                     *blank :
                     %subst(message : 1 : msgLength) :
                     msgLength :
                     msgType :
                     '*' :
                     stacklevel :
                     msgkey :
                     errords);
end-proc;


///
// \brief Send escape message
//
// This procedures sends an escape message via the system API QMHSNDPM (Send Program Message).
//
// <br><br>
//
// By default the message will be sent one level above the current stack level.
//
// \param message
// \param stack level (default: 1)
///
dcl-proc msg_sendEscapeMessage export;
  dcl-pi *N opdesc;
    message char(6000) const options(*varsize);
    callStackDepth int(10) const options(*nopass);
  end-pi;
  
  //
  // Parameters passed to CEEDOD
  //
  dcl-s descType int(10);
  dcl-s dataType int(10);
  dcl-s descInfo1 int(10);
  dcl-s descInfo2 int(10);
  dcl-s length int(10);
 
  dcl-s msgType char(10) inz('*ESCAPE');
  dcl-s msgLength int(10);
  dcl-s stacklevel int(10) inz(1);
  dcl-s msgkey char(4);
  dcl-ds errords likeds(QUSEC) inz;

  cee_getOpDescInfo( 1 : descType : dataType : descInfo1 : descInfo2 : length : *omit);
  
  msgLength = %len(%trimr(%subst(message : 1 : length)));
  
  if (%parms() = 2);
    stacklevel = callStackDepth;
  endif;
  
  sendProgramMessage('CPF9897' :
                     'QCPFMSG   QSYS      ' :
                     %subst(message : 1 : msgLength) :
                     msgLength :
                     msgType :
                     '*' :
                     stacklevel :
                     msgkey :
                     errords);
end-proc;
