**FREE

///
// \brief HTTP Request Routing Test
//
// \author Mihael Schmidt
// \date   26.11.2016
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/router_h.rpgle'
/include 'bluedroplet/router_longest_path_h.rpgle'
/include 'bluedroplet/service_h.rpgle'
/include 'bluedroplet/service_t.rpgle'
/include 'ileunit/assert_h.rpgle'
/include 'llist_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Global Variables
//-------------------------------------------------------------------------------------------------
dcl-s endPoints pointer;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr setup end-pr;
dcl-pr teardown end-pr;
dcl-pr test_exactPath end-pr;
dcl-pr test_matchingMethod end-pr;
dcl-pr test_noMatch end-pr;
dcl-pr test_longestMatchingPath end-pr;
dcl-pr test_longestMatchingPath_negative end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc test_exactPath export;
  dcl-s path char(1024) inz('/mdm/item');
  dcl-s method char(10) inz(DROPLET_GET);
  dcl-ds matchingEndPoint likeds(endpoint_t) based(ptr);
  dcl-s router pointer;
  
  router = droplet_router_longest_create();
  ptr = droplet_router_route(router : endPoints : method : path);
  droplet_router_finalize(router);
  assert_assertTrue(ptr <> *null : 'Exact match not found in end points.');
  assert_assertEquals(path : matchingEndPoint.path);
  assert_assertEquals(method : matchingEndPoint.method);
end-proc;

dcl-proc test_noMatch export;
  dcl-s path char(1024) inz('/no/match');
  dcl-s method char(10) inz(DROPLET_GET);
  dcl-ds matchingEndPoint likeds(endpoint_t) based(ptr);
  dcl-s router pointer;
  
  router = droplet_router_longest_create();
  ptr = droplet_router_route(router : endPoints : method : path);
  droplet_router_finalize(router);
  assert_assertTrue(ptr = *null : 'Returned incorrect matching end point.');
end-proc;

dcl-proc test_matchingMethod export;
  dcl-s path char(1024) inz('/mdm/item');
  dcl-s method char(10) inz(DROPLET_PUT);
  dcl-ds matchingEndPoint likeds(endpoint_t) based(ptr);
  dcl-s router pointer;
  
  router = droplet_router_longest_create();
  ptr = droplet_router_route(router : endPoints : method : path);
  droplet_router_finalize(router);
  assert_assertTrue(ptr <> *null : 'Exact match not found in end points.');
  assert_assertEquals('/mdm/item' : matchingEndPoint.path);
  assert_assertEquals(method : matchingEndPoint.method);
end-proc;

dcl-proc test_longestMatchingPath export;
  dcl-s path char(1024) inz('/mdm/item/part/12345/18');
  dcl-s method char(10) inz(DROPLET_GET);
  dcl-ds matchingEndPoint likeds(endpoint_t) based(ptr);
  dcl-s router pointer;
  
  router = droplet_router_longest_create();
  ptr = droplet_router_route(router : endPoints : method : path);
  droplet_router_finalize(router);
  assert_assertTrue(ptr <> *null : 'No match found in end points.');
  assert_assertEquals('/mdm/item/part' : matchingEndPoint.path);
  assert_assertEquals(method : matchingEndPoint.method);
end-proc;

dcl-proc test_longestMatchingPath_negative export;
  dcl-s path char(1024) inz('/mdm/item/parts');
  dcl-s method char(10) inz(DROPLET_GET);
  dcl-ds matchingEndPoint likeds(endpoint_t) based(ptr);
  dcl-s router pointer;
  
  router = droplet_router_longest_create();
  ptr = droplet_router_route(router : endPoints : method : path);
  droplet_router_finalize(router);
  assert_assertTrue(ptr <> *null : 'No match found in end points.');
  assert_assertEquals('/mdm/item' : matchingEndPoint.path);
  assert_assertEquals(method : matchingEndPoint.method);
end-proc;

dcl-proc setup export;
  dcl-ds endPoint likeds(endpoint_t) inz(*likeds);
  
  endPoints = list_create();
  
  endPoint.path = '/mdm/item';
  endPoint.method = DROPLET_GET;
  list_add(endPoints : %addr(endPoint) : %size(endPoint));
  
  clear endPoint;
  endPoint.path = '/mdm/item';
  endPoint.method = DROPLET_PUT;
  list_add(endPoints : %addr(endPoint) : %size(endPoint));
  
  clear endPoint;
  endPoint.path = '/mdm/item/part';
  endPoint.method = DROPLET_GET;
  list_add(endPoints : %addr(endPoint) : %size(endPoint));
  
  clear endPoint;
  endPoint.path = '/mdm';
  endPoint.method = DROPLET_GET;
  list_add(endPoints : %addr(endPoint) : %size(endPoint));
end-proc;

dcl-proc teardown export;
  list_dispose(endPoints);
end-proc;
