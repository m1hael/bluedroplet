**FREE

///
// \brief MIME Test
//
// \author Mihael Schmidt
// \date   19.12.2016
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/mime_h.rpgle'
/include 'ileunit/assert_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Global Variables
//-------------------------------------------------------------------------------------------------
dcl-s endPoints pointer;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr test_exactCaseMatch end-pr;
dcl-pr test_exactLoweredMatch end-pr;
dcl-pr test_noMatch end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc test_exactCaseMatch export;
  dcl-s charset char(50) inz('unicode');
  dcl-s ccsid int(10);
  
  ccsid = droplet_mime_getCcsid(charset);
  assert_equalsInteger(1200 : ccsid);
end-proc;

dcl-proc test_exactLoweredMatch export;
  dcl-s charset char(50) inz('UTF-8');
  dcl-s ccsid int(10);
  
  ccsid = droplet_mime_getCcsid(charset);
  assert_equalsInteger(1208 : ccsid);
end-proc;

dcl-proc test_noMatch export;
  dcl-s charset char(50) inz('ANSI_X3.4-1986');
  dcl-s ccsid int(10);
  
  ccsid = droplet_mime_getCcsid(charset);
  assert_equalsInteger(0 : ccsid);
end-proc;
