**FREE

///
// \brief HTTP Message Test
//
// Tests accessing the http message and parsing the query string.
// Percent encoded values will be decoded.
//
// @author Mihael Schmidt
// @date   29.11.2016
//
// @rev 24.05.2018 Mihael Schmidt
//      Added tests for query string parsing
///


ctl-opt nomain;


//
// Includes
//
/include 'bluedroplet/http_h.rpgle'
/include 'bluedroplet/mongoose_h.rpgle'
/include 'ileunit/assert_h.rpgle'
/include 'llist/llist_h.rpgle'


//
// Global Variables
//
dcl-ds httpMessageDs likeds(mongoose_httpMessage) inz(*likeds);
dcl-s httpMessage pointer;


//
// Prototypes
//
dcl-pr local_parseQueryString extproc;
  values pointer const;
  queryString pointer const;
  length int(10) const;
end-pr;

dcl-pr setupSuite end-pr;
dcl-pr teardownSuite end-pr;
dcl-pr test_simpleMessage end-pr;
dcl-pr test_headers end-pr;
dcl-pr test_querystring_empty end-pr;
dcl-pr test_querystring_singleKey end-pr;
dcl-pr test_querystring_singleKeyValue end-pr;
dcl-pr test_querystring_multiKeys end-pr;
dcl-pr test_querystring_multiKeyValues end-pr;
dcl-pr test_querystring_sameKeys end-pr;
dcl-pr test_querystring_missingKey end-pr;
dcl-pr test_queryString_missingValue end-pr;
dcl-pr test_queryString_endingAmpersand end-pr;
dcl-pr test_path_element_found end-pr;
dcl-pr test_path_element_notfound end-pr;
dcl-pr test_querystring_value_exist end-pr;
dcl-pr test_querystring_value_exist_negative end-pr;
dcl-pr test_querystring_value_notexist end-pr;
dcl-pr test_querystring_value_notexist_negative end-pr;
dcl-pr test_querystring_value_get end-pr;
dcl-pr test_querystring_value_tooshort end-pr;
dcl-pr test_querystring_value_toolong end-pr;
dcl-pr test_querystring_key_exist end-pr;
dcl-pr test_querystring_key_tooshort end-pr;
dcl-pr test_querystring_key_toolong end-pr;

//
// Procedures
//
dcl-proc test_simpleMessage export;
  dcl-s value char(100);
  dcl-ds buffer likeds(droplet_buffer);

  value = droplet_http_getMethod(httpMessage);
  assert_assertEquals('GET' : value);

  value = droplet_http_getUri(httpMessage);
  assert_assertEquals('/mdm/item/1' : value);

  value = droplet_http_getProtocol(httpMessage);
  assert_assertEquals('HTTP/1.1' : value);

  assert_equalsInteger(0 : droplet_http_getResponseCode(httpMessage));

  value = droplet_http_getResponseStatusMessage(httpMessage);
  assert_assertEquals(*blank : value);

  buffer = droplet_http_getQueryString(httpMessage);
  assert_assertEquals('content=full' : %str(buffer.string : buffer.length));

  buffer = droplet_http_getBody(httpMessage);
  value = %str(buffer.string : buffer.length);
  assert_assertEquals('{ "id"=5500 }' : value);
end-proc;


dcl-proc test_headers export;
  dcl-s value char(100);
  dcl-s names pointer;

  value = droplet_http_getHeader(httpMessage : 'Accept');
  assert_assertEquals('application/json' : value);

  names = droplet_http_getHeaderNames(httpMessage);
  assert_assertTrue(names <> *null : 'No list of header names returned.');

  value = list_getString(names : 0);
  assert_assertEquals('User-Agent' : value);
  value = list_getString(names : 1);
  assert_assertEquals('Accept' : value);

  list_dispose(names);
end-proc;


dcl-proc test_querystring_empty export;
  dcl-s queryString char(100);
  dcl-s values pointer;

  values = list_create();

  local_parseQueryString(values : %addr(queryString) : 0);

  if (not list_isEmpty(values));
    assert_fail('List of query string parts should be empty when parsing an empty query string.');
  endif;

  list_dispose(values);
end-proc;


dcl-proc test_querystring_singleKey export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'debug';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(1 : list_size(values));

  ptr = list_get(values : 0);

  assert_assertEquals('debug' : queryPart.key);
  assert_assertEquals(*blank : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_querystring_singleKeyValue export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'scope=full';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(1 : list_size(values));

  ptr = list_get(values : 0);

  assert_assertEquals('scope' : queryPart.key);
  assert_assertEquals('full' : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_querystring_multiKeys export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'skip&debug';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(2 : list_size(values));

  ptr = list_get(values : 0);
  assert_assertEquals('skip' : queryPart.key);
  assert_assertEquals(*blank : queryPart.value);

  ptr = list_get(values : 1);
  assert_assertEquals('debug' : queryPart.key);
  assert_assertEquals(*blank : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_querystring_multiKeyValues export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'scope=full&debug=true&transport=async';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(3 : list_size(values));

  ptr = list_get(values : 0);
  assert_assertEquals('scope' : queryPart.key);
  assert_assertEquals('full' : queryPart.value);

  ptr = list_get(values : 1);
  assert_assertEquals('debug' : queryPart.key);
  assert_assertEquals('true' : queryPart.value);

  ptr = list_get(values : 2);
  assert_assertEquals('transport' : queryPart.key);
  assert_assertEquals('async' : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_querystring_sameKeys export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'postalcode=32425&postalcode=32457';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(2 : list_size(values));

  ptr = list_get(values : 0);
  assert_assertEquals('postalcode' : queryPart.key);
  assert_assertEquals('32425' : queryPart.value);

  ptr = list_get(values : 1);
  assert_assertEquals('postalcode' : queryPart.key);
  assert_assertEquals('32457' : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_querystring_missingKey export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'postalcode=32425&=32457';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(2 : list_size(values));

  ptr = list_get(values : 0);
  assert_assertEquals('postalcode' : queryPart.key);
  assert_assertEquals('32425' : queryPart.value);

  ptr = list_get(values : 1);
  assert_assertEquals(*blank : queryPart.key);
  assert_assertEquals('32457' : queryPart.value);

  list_dispose(values);
end-proc;

dcl-proc test_queryString_missingValue export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'skip=&debug=';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(2 : list_size(values));

  ptr = list_get(values : 0);
  assert_assertEquals('skip' : queryPart.key);
  assert_assertEquals(*blank : queryPart.value);

  ptr = list_get(values : 1);
  assert_assertEquals('debug' : queryPart.key);
  assert_assertEquals(*blank : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_queryString_endingAmpersand export;
  dcl-s queryString char(100);
  dcl-s values pointer;
  dcl-ds queryPart likeds(http_queryPart_t) based(ptr);

  values = list_create();

  queryString = 'skip=&';

  local_parseQueryString(values : %addr(queryString) : %len(%trimr(queryString)));
  assert_equalsInteger(1 : list_size(values));

  ptr = list_get(values : 0);
  assert_assertEquals('skip' : queryPart.key);
  assert_assertEquals(*blank : queryPart.value);

  list_dispose(values);
end-proc;


dcl-proc test_path_element_found export;
  dcl-s element char(1024);

  element = droplet_http_getPathElement(httpMessage : 1);
  assert_assertEquals('mdm' : element);

  element = droplet_http_getPathElement(httpMessage : 2);
  assert_assertEquals('item' : element);

  element = droplet_http_getPathElement(httpMessage : 3);
  assert_assertEquals('1' : element);
end-proc;


dcl-proc test_path_element_notfound export;
  dcl-s element char(1024);

  element = droplet_http_getPathElement(httpMessage : 0);
  assert_assertEquals(*blank : element);

  element = droplet_http_getPathElement(httpMessage : 4);
  assert_assertEquals(*blank : element);
end-proc;


dcl-proc test_querystring_value_get export;
  dcl-s value char(1024);

  value = droplet_http_getQueryValue(httpMessage : 'content');
  assert_assertEquals('full' : value);
end-proc;


dcl-proc test_querystring_value_tooshort export;
  dcl-s value char(1024);

  value = droplet_http_getQueryValue(httpMessage : 'cont');
  assert_assertEquals(*blank : value);
end-proc;


dcl-proc test_querystring_value_toolong export;
  dcl-s value char(1024);

  value = droplet_http_getQueryValue(httpMessage : 'contents');
  assert_assertEquals(*blank : value);
end-proc;


dcl-proc test_querystring_key_exist export;
  assert_assertTrue(droplet_http_containsQueryKey(httpMessage : 'content') :
                    'url query string contains query key "content"');
end-proc;


dcl-proc test_querystring_key_tooshort export;
  assert_assertTrue(not droplet_http_containsQueryKey(httpMessage : 'cont') :
                    'url query string does not contain query key "cont"');
end-proc;


dcl-proc test_querystring_key_toolong export;
  assert_assertTrue(not droplet_http_containsQueryKey(httpMessage : 'contents') :
                    'url query string does not contain query key "contents"');
end-proc;


dcl-proc setup export;
  httpMessage = %addr(httpMessageDs);

  httpMessageDs.message.value = %alloc(38);
  %str(httpMessageDs.message.value : 38) = 'GET /mdm/item/1?content=full HTTP/1.1';
  httpMessageDs.message.length = 37;

  httpMessageDs.method.value = %alloc(4);
  %str(httpMessageDs.method.value : 4) = 'GET';
  httpMessageDs.method.length = 3;

  httpMessageDs.uri.value = %alloc(12);
  %str(httpMessageDs.uri.value : 12) = '/mdm/item/1';
  httpMessageDs.uri.length = 11;

  httpMessageDs.protocol.value = %alloc(9);
  %str(httpMessageDs.protocol.value : 9) = 'HTTP/1.1';
  httpMessageDs.protocol.length = 8;

  httpMessageDs.responseCode = 0;
  httpMessageDs.responseStatusMessage.length = 0;

  httpMessageDs.queryString.value = %alloc(13);
  %str(httpMessageDs.queryString.value : 13) = 'content=full';
  httpMessageDs.queryString.length = 12;

  clear httpMessageDs.headerNames;
  clear httpMessageDs.headerValues;
  httpMessageDs.headerNames(1).value = %alloc(11);
  %str(httpMessageDs.headerNames(1).value : 11) = 'User-Agent';
  httpMessageDs.headerNames(1).length = 10;
  httpMessageDs.headerValues(1).value = %alloc(12);
  %str(httpMessageDs.headerValues(1).value : 12) = 'curl/7.26.0';
  httpMessageDs.headerValues(1).length = 11;
  httpMessageDs.headerNames(2).value = %alloc(7);
  %str(httpMessageDs.headerNames(2).value : 7) = 'Accept';
  httpMessageDs.headerNames(2).length = 6;
  httpMessageDs.headerValues(2).value = %alloc(17);
  %str(httpMessageDs.headerValues(2).value : 17) = 'application/json';
  httpMessageDs.headerValues(2).length = 16;

  httpMessageDs.body.value = %alloc(14);
  %str(httpMessageDs.body.value : 14) = '{ "id"=5500 }';
  httpMessageDs.body.length = 13;
end-proc;

dcl-proc teardown export;
  dcl-s ptr pointer;

  ptr = httpMessageDs.message.value;
  dealloc ptr;
  ptr = httpMessageDs.method.value;
  dealloc ptr;
  ptr = httpMessageDs.uri.value;
  dealloc ptr;
  ptr = httpMessageDs.protocol.value;
  dealloc ptr;
  ptr = httpMessageDs.queryString.value;
  dealloc ptr;
  ptr = httpMessageDs.headerNames(1).value;
  dealloc ptr;
  ptr = httpMessageDs.headerNames(2).value;
  dealloc ptr;
  ptr = httpMessageDs.headerValues(1).value;
  dealloc ptr;
  ptr = httpMessageDs.headerValues(2).value;
  dealloc ptr;
  ptr = httpMessageDs.body.value;
  dealloc ptr;
end-proc;
