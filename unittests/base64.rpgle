**FREE

///
// \brief Base64 Test
//
// Note: Using https://www.base64encode.org/ to get some test data.
//
// \author Mihael Schmidt
// \date   05.12.2016
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/base64/base64_h.rpgle'
/include 'ileunit/assert_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr test_encode end-pr;
dcl-pr test_encodeUmlaute end-pr;
dcl-pr test_decode end-pr;
dcl-pr test_decodeUmlaute end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc test_encode export;
  dcl-s value char(100) ccsid(819);
  dcl-s encoded char(100) based(ptr);
  dcl-s encodedSize int(10);
  
  value = 'The big brown fox jumps over the lazy dog.';
  ptr = base64_encode(%addr(value) : %len(%trimr(value)) : %addr(encodedSize));
  assert_assertEquals('VGhlIGJpZyBicm93biBmb3gganVtcHMgb3ZlciB0aGUgbGF6eSBkb2cu' : 
    %subst(encoded : 1 : encodedSize-1)); // -1 because of the null termination char
  dealloc(n) ptr;
  
  value = 'The big brown fox jumps over the lazy dog.1';
  ptr = base64_encode(%addr(value) : %len(%trimr(value)) : %addr(encodedSize));
  assert_assertEquals('VGhlIGJpZyBicm93biBmb3gganVtcHMgb3ZlciB0aGUgbGF6eSBkb2cuMQ==' : 
    %subst(encoded : 1 : encodedSize-1)); // -1 because of the null termination char
  dealloc(n) ptr;  
    
  value = 'The big brown fox jumps over the lazy dog.12';
  ptr = base64_encode(%addr(value) : %len(%trimr(value)) : %addr(encodedSize));
  assert_assertEquals('VGhlIGJpZyBicm93biBmb3gganVtcHMgb3ZlciB0aGUgbGF6eSBkb2cuMTI=' :
    %subst(encoded : 1 : encodedSize-1)); // -1 because of the null termination char
  dealloc(n) ptr;
end-proc;

dcl-proc test_encodeUmlaute export;
  dcl-s value char(100) ccsid(819);
  dcl-s encoded char(100) based(ptr);
  dcl-s encodedSize uns(10);
  
  value = 'Umlaute ';
  ptr = base64_encode(%addr(value) : %len(%trimr(value)) : %addr(encodedSize));
  assert_assertEquals('VW1sYXV0ZSDDpMO2w7w=' : %subst(encoded : 1 : encodedSize-1));
  dealloc(n) ptr;
end-proc;

dcl-proc test_decode export;
  dcl-s encoded char(50) ccsid(819);
  dcl-s encodedSize uns(10);
  dcl-s decoded char(50) based(decodedPtr) ccsid(819);
  dcl-s decodedSize uns(10);
  
  encoded = 'bS5zY2htaWR0Om15c2VjcmV0MzU4';
  encodedSize = %len(%trimr(encoded));
  decodedPtr = base64_decode(%addr(encoded) : encodedSize : %addr(decodedSize));
  assert_assertEquals('m.schmidt:mysecret358' : %subst(decoded : 1 : decodedSize));
  dealloc(n) decodedPtr;
end-proc;

dcl-proc test_decodeUmlaute export;
  dcl-s encoded char(50) ccsid(819);
  dcl-s encodedSize uns(10);
  dcl-s decoded char(50) based(decodedPtr) ccsid(819);
  dcl-s decodedSize uns(10);
  
  encoded = 'VW1sYXV0ZSDDpMO2w7w=';
  encodedSize = %len(%trimr(encoded));
  decodedPtr = base64_decode(%addr(encoded) : encodedSize : %addr(decodedSize));
  assert_assertEquals('Umlaute ' : %subst(decoded : 1 : decodedSize));
  dealloc(n) decodedPtr;
end-proc;
