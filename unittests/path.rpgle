**FREE

///
// \brief HTTP URI Parsing
//
// \author Mihael Schmidt
// \date   31.12.2016
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/service_h.rpgle'
/include 'ileunit/assert_h.rpgle'
/include 'llist_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr test_index_rootOnly end-pr;
dcl-pr test_index_outOfBounds end-pr;
dcl-pr test_index_positive end-pr;
dcl-pr test_index_withQuery end-pr;
dcl-pr test_index_withFragment end-pr;
dcl-pr test_index_withQueryAndFragment end-pr;
dcl-pr test_index_encodedPath end-pr;
dcl-pr test_index_encodedPathWithQueryAndFragment end-pr;
dcl-pr test_last_rootOnly end-pr;
dcl-pr test_last_positive end-pr;
dcl-pr test_last_withQuery end-pr;
dcl-pr test_last_withFragment end-pr;
dcl-pr test_last_withQueryAndFragment end-pr;
dcl-pr test_last_encodedPath end-pr;
dcl-pr test_last_encodedPathWithQueryAndFragment end-pr;
dcl-pr test_list_rootOnly end-pr;
dcl-pr test_list_positive end-pr;
dcl-pr test_list_positiveEncoded end-pr;
dcl-pr test_list_emptySegment end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc test_index_rootOnly export;
  dcl-s path char(1024) inz('/');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 1 : 0);
  assert_assertEquals(*blank : segment);
end-proc;

dcl-proc test_index_outOfBounds export;
  dcl-s path char(1024) inz('/mdm/item');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 9 : 5);
  assert_assertEquals(*blank : segment);
end-proc;

dcl-proc test_index_positive export;
  dcl-s path char(1024) inz('/mdm/item');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 9 : 1);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_index_withQuery export;
  dcl-s path char(1024) inz('/mdm/item?details=all');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 21 : 1);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_index_withFragment export;
  dcl-s path char(1024) inz('/mdm/item#dummy');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 15 : 1);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_index_withQueryAndFragment export;
  dcl-s path char(1024) inz('/mdm/item?details=all#dummy');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 27 : 1);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_index_encodedPath export;
  dcl-s path char(1024) inz('%2Fmdm%2Fitem');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 13 : 1);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_index_encodedPathWithQueryAndFragment export;
  dcl-s path char(1024) inz('%2Fmdm%2Fitem%3Fdetails%3Dall%23dummy');
  dcl-s segment char(1024);
  
  segment = droplet_service_getPathSegment(%addr(path) : 37 : 1);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_last_rootOnly export;
  dcl-s path char(1024) inz('/');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 1);
  assert_assertEquals(*blank : segment);
end-proc;

dcl-proc test_last_positive export;
  dcl-s path char(1024) inz('/mdm/item');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 9);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_last_withQuery export;
  dcl-s path char(1024) inz('/mdm/item?details=all');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 21);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_last_withFragment export;
  dcl-s path char(1024) inz('/mdm/item#dummy');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 15);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_last_withQueryAndFragment export;
  dcl-s path char(1024) inz('/mdm/item?details=all#dummy');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 27);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_last_encodedPath export;
  dcl-s path char(1024) inz('%2Fmdm%2Fitem');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 13);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_last_encodedPathWithQueryAndFragment export;
  dcl-s path char(1024) inz('%2Fmdm%2Fitem%3Fdetails%3Dall%23dummy');
  dcl-s segment char(1024);
  
  segment = droplet_service_getLastPathSegment(%addr(path) : 37);
  assert_assertEquals('item' : segment);
end-proc;

dcl-proc test_list_rootOnly export;
  dcl-s path char(1024) inz('/');
  dcl-s segments pointer;
  
  segments = droplet_service_splitPath(%addr(path) : 1);
  assert_assertTrue(list_isEmpty(segments) : 
    'List of segments should have been empty but holds ' + 
    %char(list_size(segments)) + ' segments: + ' + %trimr(list_toString(segments)) + '.');
  
  list_dispose(segments);
end-proc;

dcl-proc test_list_positive export;
  dcl-s path char(1024) inz('/mdm/item');
  dcl-s segments pointer;
  
  segments = droplet_service_splitPath(%addr(path) : 9);
  assert_equalsInteger(2 : list_size(segments));
  assert_assertEquals('mdm' : list_getString(segments : 0));
  assert_assertEquals('item' : list_getString(segments : 1));
  
  list_dispose(segments);
end-proc;

dcl-proc test_list_positiveEncoded export;
  dcl-s path char(1024) inz('%2Fmdm%2Fitem');
  dcl-s segments pointer;
  
  segments = droplet_service_splitPath(%addr(path) : 13);
  assert_equalsInteger(2 : list_size(segments));
  assert_assertEquals('mdm' : list_getString(segments : 0));
  assert_assertEquals('item' : list_getString(segments : 1));
  
  list_dispose(segments);
end-proc;

dcl-proc test_list_emptySegment export;
  dcl-s path char(1024) inz('//no/good/url');
  dcl-s segments pointer;
  
  segments = droplet_service_splitPath(%addr(path) : 13);
  assert_equalsInteger(3 : list_size(segments));
  assert_assertEquals('no' : list_getString(segments : 0));
  assert_assertEquals('good' : list_getString(segments : 1));
  assert_assertEquals('url' : list_getString(segments : 2));
  
  list_dispose(segments);
end-proc;
