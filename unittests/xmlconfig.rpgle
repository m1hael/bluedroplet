**FREE

///
// \brief XML Configuration Loading Test
//
// \author Mihael Schmidt
// \date   09.11.2016
///


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Includes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/xml_config_provider_h.rpgle'
/include 'ileunit/assert_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr test_xmlConfig end-pr;
dcl-pr test_xmlConfigDefaults end-pr;
     
     
//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc test_xmlConfig export;

  dcl-ds configuration likeds(droplet_config_configuration);
  dcl-s configFile char(1024);
  
  configFile = 'config.xml';
  monitor;
    configuration = droplet_config_xml_load(configFile);
  on-error *all;
    assert_fail('Error on reading xml config file config.xml. You need to set '+
                'the current directory to the unit test directory with CHGCURDIR.');
  endmon;
  
  assert_assertEquals('127.0.0.1' : %trimr(configuration.httpAddress));
  assert_equalsInteger(35800 : configuration.httpPort);
  assert_assertEquals('172.16.1.1' : %trimr(configuration.adminAddress));
  assert_equalsInteger(35801 : configuration.adminPort);
  assert_assertTrue(configuration.logging = *on : 'Logging is enabled.');
  assert_assertEquals('BDLOGSTMF' : configuration.loggerServiceProgram);
  assert_assertEquals('droplet_logger_simple_create' : configuration.loggerCreateProcedure);
  assert_assertEquals('/var/local/log/droplet.log' : configuration.loggerUserData);
  assert_assertEquals('/var/local/templates' : configuration.templatePath);
  assert_equalsInteger(500 : configuration.pollInterval);
  assert_assertEquals('MSCHMIDT' : configuration.controlLibrary);
  assert_assertEquals('DROPLET' : configuration.controlDataArea);
  assert_assertTrue(configuration.addToLibraryList = *off : 'Adding libraries instead of replacing libraries list.');
  assert_assertEquals('MSCHMIDT1' : configuration.libraries(1));
  assert_assertEquals('MSCHMIDT2' : configuration.libraries(2));
  assert_assertTrue(configuration.libraries(3) = *blank : 'Library set instead of *blank');
end-proc;

dcl-proc test_xmlConfigDefaults export;

  dcl-ds configuration likeds(droplet_config_configuration);
  dcl-s configFile char(1024);
  
  configFile = 'config-defaults.xml';
  monitor;
    configuration = droplet_config_xml_load(configFile);
  on-error *all;
    assert_fail('Error on reading xml config file config.xml. You need to set '+
                'the current directory to the unit test directory with CHGCURDIR.');
  endmon;
  
  assert_assertEquals('0.0.0.0' : %trimr(configuration.httpAddress));
  assert_equalsInteger(8484 : configuration.httpPort);
  assert_assertEquals('0.0.0.0' : %trimr(configuration.adminAddress));
  assert_equalsInteger(9494 : configuration.adminPort);
  assert_assertTrue(configuration.logging = *on : 'Logging is not disabled.');
  assert_assertEquals('' : %trimr(configuration.templatePath));
  assert_equalsInteger(1000 : configuration.pollInterval);
  assert_assertEquals(*blank : configuration.controlLibrary);
  assert_assertEquals(*blank : configuration.controlDataArea);
  assert_assertTrue(configuration.addToLibraryList = *on : 'Replacing library list instead of adding libraries.');
  assert_assertTrue(configuration.libraries(1) = *blank : 'Library set instead of *blank');
end-proc;
