**FREE

/if not defined (BLUEDROPLET_ROUTER_LONGEST_PATH_H)
/define BLUEDROPLET_ROUTER_LONGEST_PATH_H

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2017 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------

dcl-pr droplet_router_longest_create pointer extproc('droplet_router_longest_create');
  userData pointer const options(*nopass);
end-pr;

dcl-pr droplet_router_longest_route pointer extproc('droplet_router_longest_route');
  router pointer const;
  endPoints pointer const;
  method char(8) const;
  path char(1024) const;
end-pr;

dcl-pr droplet_router_longest_finalize extproc('droplet_router_longest_finalize');
  router pointer;
end-pr;

/endif
