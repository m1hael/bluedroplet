**FREE

//
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//


ctl-opt nomain;

/include 'endpoint_h.rpgle'

dcl-proc droplet_endpoint_getPath export;
  dcl-pi *n char(1024);
    endpoint pointer const;
  end-pi;

  dcl-ds endpointDs likeds(endpoint_t) based(endpoint);

  return endpointDs.path;
end-proc;

dcl-proc droplet_endpoint_getMethod export;
  dcl-pi *n char(10);
    endpoint pointer const;
  end-pi;

  dcl-ds endpointDs likeds(endpoint_t) based(endpoint);

  return endpointDs.method;
end-proc;

