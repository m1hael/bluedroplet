**FREE

/if not defined(BLUEDROPLET_MESSAGE_H)
/define BLUEDROPLET_MESSAGE_H

//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr msg_sendProgramMessage extproc('msg_sendProgramMessage') opdesc;
  message char(6000) const options(*varsize);
  messageType char(10) const options(*omit : *nopass);
  callStackDepth int(10) const options(*nopass);
end-pr;

dcl-pr msg_sendEscapeMessage extproc('msg_sendEscapeMessage') opdesc;
  message char(6000) const options(*varsize);
  callStackDepth int(10) const options(*nopass);
end-pr;


//-------------------------------------------------------------------------------------------------
// Constants
//-------------------------------------------------------------------------------------------------
dcl-c MESSAGE_TYPE_INFO   '*INFO     ';
dcl-c MESSAGE_TYPE_DIAG   '*DIAG     ';
dcl-c MESSAGE_TYPE_ESCAPE '*ESCAPE   ';

/endif
