**FREE

///
// \brief BlueDroplet : Utilities
//
// This contains some utility procedures.
//
// \author Mihael Schmidt
// \date   19.12.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'iconv_h.rpgle'
/include 'util_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-proc droplet_util_createConversionDescriptor export;
  dcl-pi *N likeds(iconv_t);
    fromCcsid int(10) const;
    toCcsid int(10) const;
  end-pi;
  
  dcl-ds descriptor likeds(iconv_t);
  dcl-ds to likeds(iconv_op_t) inz(*likeds);
  dcl-ds from likeds(iconv_op_t) inz(*likeds);
  
  from.ccsid = fromCcsid;
  to.ccsid = toCcsid;
  
  descriptor = iconv_open(to : from);
  
  return descriptor;
end-proc;

dcl-proc droplet_util_getCharsetFromHttpHeader export;
  dcl-pi *N char(50);
    headerEntry char(100) const;
  end-pi;
  
  dcl-s charset char(50);
  dcl-s charsetIndex int(10);
  
  charsetIndex = %scan('charset=' : headerEntry);
  if (charsetIndex <> 0);
    charsetIndex += 8; // move to the charset value
    charset = %subst(headerEntry : charsetIndex);
    // TODO remove any trailing additional attribute
  endif;
  
  return charset;
end-proc;