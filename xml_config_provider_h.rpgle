**FREE

/if not defined (BLUEDROPLET_CONFIG_XML_H)
/define BLUEDROPLET_CONFIG_XML_H

///
// \brief BlueDroplet : XML Configuration Provider
//
// \author Mihael Schmidt
// \date   08.11.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'config_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr droplet_config_xml_load likeds(droplet_config_configuration) extproc('droplet_config_xml_load');
  path char(1024) const;
end-pr;

/endif
