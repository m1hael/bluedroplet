**FREE

/if not defined (BLUEDROPLET_H)
/define BLUEDROPLET_H

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Data Structures
//-------------------------------------------------------------------------------------------------
/include 'config_t.rpgle'
/include 'logger_t.rpgle'
/include 'service_t.rpgle'
/include 'router_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'service_h.rpgle'
/include 'http_h.rpgle'
/include 'mime_h.rpgle'
/include 'util_h.rpgle'
/include 'logger_h.rpgle'
/include 'simple_logger_h.rpgle'
/include 'xml_config_provider_h.rpgle'
/include 'llist/llist_h.rpgle'
/include 'endpoint_h.rpgle'

/endif
