**FREE

///
// \brief BlueDroplet : XML Configuration Loader
//
// This modules contains the procedures for loading a xml BlueDroplet 
// configuration file via the xml bifs and opcodes introduced in V5R4.
//
// \author Mihael Schmidt
// \date   05.11.2016
//
///

//-------------------------------------------------------------------------------------------------
//
// (C) Copyleft 2016 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//-------------------------------------------------------------------------------------------------


ctl-opt nomain;


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
dcl-pr xmlHandler int(10);
  commArea likeds(tmpl_xmldata);
  event int(10) value;
  string pointer value;
  stringLen int(20) value;
  exceptionId int(10) value;
end-pr;

/include 'xml_config_provider_h.rpgle'
/include 'message_h.rpgle'
/include 'libc_h.rpgle'


//-------------------------------------------------------------------------------------------------
// Variables
//-------------------------------------------------------------------------------------------------
dcl-ds tmpl_xmldata  qualified template;
  configuration likeds(droplet_config_configuration);
  lastAttrName char(100);
  lastElement char(100);
  numberLibraries int(10);
end-ds;
 
/include 'config_t.rpgle'


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------

///
// \brief Load configuration from XML file
//
// \param XML configuration file path
//
// \throws XML Parse error
///
dcl-proc droplet_config_xml_load export;
  dcl-pi *N likeds(droplet_config_configuration);
    file char(1024) const;
  end-pi;
  
  dcl-s retVal ind inz(*on);
  dcl-ds xmlCommArea likeds(tmpl_xmldata);
  xmlCommArea.numberLibraries = 0;

  // initialize configuration data structure
  xmlCommArea.configuration.httpAddress = '0.0.0.0';
  xmlCommArea.configuration.httpPort = 8484;
  xmlCommArea.configuration.adminAddress = '0.0.0.0';
  xmlCommArea.configuration.adminPort = 9494;
  xmlCommArea.configuration.logging = *on;
  xmlCommArea.configuration.templatePath = *blank;
  xmlCommArea.configuration.pollInterval = 1000;
  xmlCommArea.configuration.addToLibraryList = *on;

  xml-sax %handler(xmlHandler : xmlCommArea) %xml(%trimr(file) : 'doc=file');
  
  return xmlCommArea.configuration;
end-proc;


///
// \brief XML handler procedure for config loading
///
dcl-proc xmlHandler;
  dcl-pi *N int(10);
    commArea likeds(tmpl_xmldata);
    event int(10) value;
    string pointer value;
    stringLen int(20) value;
    exceptionId int(10) value;
  end-pi;

  dcl-s value char(1000) based(string);
  dcl-s tmp char(100);
  dcl-s retVal int(10);
  dcl-s fieldLength uns(10);
  
  select;
    when (event = *XML_START_ELEMENT);
      commArea.lastElement = %subst(value : 1 : stringLen);
    
    when (event = *XML_CHARS);
      if (commArea.lastElement = 'logger');
        // store user data null terminated for easier processing
        commArea.configuration.loggerUserData = %trim(%subst(value : 1 : stringLen)) + x'00';
      elseif (commArea.lastElement = 'library');
        commArea.numberLibraries += 1;
        commArea.configuration.libraries(commArea.numberLibraries) =%trim(%subst(value : 1 : stringLen));
      endif;
    
    when (event = *XML_ATTR_NAME);
      commArea.lastAttrName = %subst(value : 1 : stringLen);

    when (event = *XML_ATTR_CHARS);
      if (commArea.lastElement = 'server');
        if (commArea.lastAttrName = 'address');
          commArea.configuration.httpAddress = %subst(value : 1 : stringLen);
        elseif (commArea.lastAttrName = 'port');
          commArea.configuration.httpPort = %int(%subst(value : 1 : stringLen));
        elseif (commArea.lastAttrName = 'pollInterval');
          commArea.configuration.pollInterval = %int(%subst(value : 1 : stringLen));
        endif;
      elseif (commArea.lastElement = 'admin');
        if (commArea.lastAttrName = 'address');
          commArea.configuration.adminAddress = %subst(value : 1 : stringLen);
        elseif (commArea.lastAttrName = 'port');
          commArea.configuration.adminPort = %int(%subst(value : 1 : stringLen));
        endif;
      elseif (commArea.lastElement = 'templates');
        if (commArea.lastAttrName = 'path');
          commArea.configuration.templatePath = %subst(value : 1 : stringLen);
        endif;
      elseif (commArea.lastElement = 'logging');
        if (commArea.lastAttrName = 'enabled');
          commArea.configuration.logging = %subst(value : 1 : stringLen) = 'true';
        elseif (commArea.lastAttrName = 'level');
          commArea.configuration.logLevel = %subst(value : 1 : stringLen);
        endif;
      elseif (commArea.lastElement = 'logger');
        if (commArea.lastAttrName = 'serviceProgram');
          commArea.configuration.loggerServiceProgram = %subst(value : 1 : stringLen);
        elseif (commArea.lastAttrName = 'createProcedure');
          commArea.configuration.loggerCreateProcedure = %subst(value : 1 : stringLen);
        endif;
      elseif (commArea.lastElement = 'control');
        if (commArea.lastAttrName = 'library');
          commArea.configuration.controlLibrary = %subst(value : 1 : stringLen);
        elseif (commArea.lastAttrName = 'dataarea');
          commArea.configuration.controlDataArea = %subst(value : 1 : stringLen);
        endif;
      elseif (commArea.lastElement = 'librarylist');
        if (commArea.lastAttrName = 'addToLibraryList');
          commArea.configuration.addToLibraryList = %subst(value : 1 : stringLen) = 'true';
        endif;
      endif;
    
    when (event = *XML_END_ELEMENT);
      commArea.lastAttrName = *blank;
      
  endsl;

  return retVal;
end-proc;
