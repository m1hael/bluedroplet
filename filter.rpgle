**FREE

///
// \brief BlueDroplet : Filter Interface
//
// This module implements an interface for filter. If a filter or a set of 
// filters (chained filters) is registered at the service instance all HTTP
// requests which can be routed to an existing endpoint will be passedd through
// the filter(s). The filter may send an HTTP response instead of passing the 
// request to an endpoint (f. e. if the requestor cannot be authenticated).
//
// \author Mihael Schmidt
// \date   28.08.2018
//
///

//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

ctl-opt nomain;


//
// Prototypes
//
/include 'filter_h.rpgle'


//
// Procedures
//

dcl-proc droplet_filter_filterRequest export;
  dcl-pi *n ind;
    filter pointer const;
    service pointer const;
    connection pointer const;
    httpMessage pointer const;
  end-pi;
  
  dcl-ds filterDs likeds(filter_t) based(filter);
  dcl-s filterPointer pointer(*proc);
  dcl-pr filterRequest ind extproc(filterPointer);
    filter pointer const;
    service pointer const;
    connection pointer const;
    httpMessage pointer const;
  end-pr;
  
  filterPointer = filterDs.proc_filterRequest;
  return filterRequest(filter : service : connection : httpMessage);
end-proc;

dcl-proc droplet_filter_finalize export;
  dcl-pi *n;
    filter pointer;
  end-pi;
  
  dcl-ds filterDs likeds(filter_t) based(filter);
  dcl-s finalizePointer pointer(*proc);
  dcl-pr finalize extproc(finalizePointer);
    filter pointer;
  end-pr;
  
  if (filter = *null);
    return;
  endif;
  
  finalizePointer = filterDs.proc_finalize;
  
  finalize(filter);
  
  if (filter <> *null);
    dealloc(n) filter;
  endif;
end-proc;
