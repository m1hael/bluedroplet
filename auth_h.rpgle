**FREE

/if not defined (BLUEDROPLET_AUTH)
/define BLUEDROPLET_AUTH


//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

dcl-c DROPLET_AUTH_BASIC 1;
dcl-c DROPLET_AUTH_TOKEN 2;

dcl-ds droplet_auth_basic_t qualified template;
  username char(1024);
  password char(1024);
end-ds;

dcl-ds droplet_auth_token_t qualified template;
  token pointer;
  tokenLength int(10);
end-ds;

dcl-ds authenticator_t qualified template;
  id char(50);
  userData pointer;
  proc_authenticate pointer(*proc);
  proc_finalize pointer(*proc);
end-ds;

dcl-pr droplet_auth_authenticate ind extproc(*dclcase);
  authenticator pointer const;
  credentials pointer const;
  type int(5) const;
end-pr;

dcl-pr droplet_auth_finalize extproc(*dclcase);
  authenticator pointer;
end-pr;

/endif

