**FREE

/if not defined (BLUEDROPLET_FILTER)
/define BLUEDROPLET_FILTER


//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

dcl-ds filter_t qualified template;
  id char(50);
  userData pointer;
  chainedFilter pointer;
  proc_filterRequest pointer(*proc);
  proc_finalize pointer(*proc);
end-ds;

dcl-pr droplet_filter_filterRequest ind extproc(*dclcase);
  filter pointer const;
  service pointer const;
  connection pointer const;
  httpMessage pointer const;
end-pr;

dcl-pr droplet_filter_finalize extproc(*dclcase);
  filter pointer;
end-pr;

/endif

