**FREE

///
// \brief BlueDroplet : Basic Auth Filter
//
// This module implements the filter interface. It parses the HTTP header for
// authentication data and passed it to the registered authenticator.
//
// \author Mihael Schmidt
// \date   28.08.2018
//
///

//
//
// (C) Copyleft 2018 Mihael Schmidt
//
// This file is part of BlueDroplet.
//
// BlueDroplet is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// BlueDroplet is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BlueDroplet.  If not, see <http://www.gnu.org/licenses/>.
//
//

ctl-opt nomain;


//
// Prototypes
//
/include 'auth_h.rpgle'
/include 'basicauthfilter_h.rpgle'
/include 'filter_h.rpgle'
/include 'http_h.rpgle'
/include 'service_h.rpgle'
/include 'base64/base64_h.rpgle'


//
// Procedures
//
dcl-proc droplet_filter_basic_auth_create export;
  dcl-pi *n pointer;
    chainedFilter pointer const options(*omit : *nopass);
    userdata pointer const options(*nopass : *string);
  end-pi;
  
  dcl-s filter pointer;
  dcl-ds filterDs likeds(filter_t) based(filter);
  
  filter = %alloc(%size(filter_t));
  filterDs.id = 'droplet_filter_basic_auth';
  filterDs.proc_filterRequest = %paddr('droplet_filter_basic_auth_filterRequest');
  filterDs.proc_finalize = %paddr('droplet_filter_basic_auth_finalize');
  
  if (%parms() > 0 and %addr(chainedFilter) <> *null);
    filterDs.chainedFilter = chainedFilter;
  endif;
  
  return filter;
end-proc;


dcl-proc droplet_filter_basic_auth_filterRequest export;
  dcl-pi *n ind;
    filter pointer const;
    service pointer const;
    connection pointer const;
    httpMessage pointer const;
  end-pi;
  
  dcl-ds filterDs likeds(filter_t) based(filter);
  dcl-s authenticator pointer;
  dcl-ds basicCreds likeds(droplet_auth_basic_t);
  dcl-s headerValue char(1024);
  
  authenticator = droplet_service_getAuthenticator(service);
  if (authenticator = *null);
    return *on;
  endif;
  
  // TODO header name are caes INsensitive
  headerValue = droplet_http_getHeader(httpMessage : 'Authorization');
  // TODO
  
  if (not droplet_auth_authenticate(authenticator : %addr(basicCreds) : DROPLET_AUTH_BASIC));
    droplet_service_send(connection : DROPLET_UNAUTHORIZED);
    return *off;
  else;
    
    if (filterDs.chainedFilter <> *null);
      return droplet_filter_filterRequest(filterDs.chainedFilter : service : 
        connection : httpMessage);
    else;
      return *on;
    endif;
    
  endif;
end-proc;


dcl-proc droplet_filter_basic_auth_finalize export;
  dcl-pi *n;
    filter pointer;
  end-pi;
  
  dealloc(n) filter;
end-proc;
