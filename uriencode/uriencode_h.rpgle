**FREE

/if not defined (URIENCODE)
/define URIENCODE

dcl-pr uri_encode int(10) extproc(*dclcase);
  value pointer value;
  length int(10) value;
  dest pointer value;
end-pr;

dcl-pr uri_decode int(10) extproc(*dclcase);
  value pointer value;
  length int(10) value;
  dest pointer value;
end-pr;

/endif
